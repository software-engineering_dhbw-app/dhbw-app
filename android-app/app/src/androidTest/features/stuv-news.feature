Feature:
  In order to see the latest news from the student body
  As an app user
  I want to be see a short overview with the latest headlines

  @Minimal
  Scenario: Show headlines without using cache
    Given I have the app open.
    And The information isn't available in the "news" cache.
    And I have an internet connection.
    Then I should see the headlines.
    And The information is saved in the "news" cache.

  @Offline
  Scenario: Show headlines with using cache
    Given I have the app open.
    And The information is available in the "news" cache.
    Then I should see the headlines.

  @Error
  Scenario: Can't show headlines
    Given I have the app open.
    And The information isn't available in the "news" cache.
    And I don't have an internet connection.
    Then I shouldn't see the headlines (and the corresponding card).
