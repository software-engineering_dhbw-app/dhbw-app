Feature:
  In order to contact employee of the university
  As an app user
  I want to be see a list of all employees

  @Minimal
  Scenario: Show list of employees without using cache
    # Given I have an internet connection.
    # And The information isn't available in the "employee" cache.
    Then I touch the "Kontaktperson" text
    Then I drag from 50:10 to 50:80 moving with 1 steps
    Then I should see text containing "Heinrich, Braun"

  @Offline
  Scenario: Show list of employees with using cache
    Given I have the app open.
    And The information is available in the "employee" cache.
    Then I should see the list of employees.

  @Error
  Scenario: Can't show list of employees
    Given I have the app open.
    And The information isn't available in the "employee" cache.
    And I don't have an internet connection.
    Then I should see an error message.

  