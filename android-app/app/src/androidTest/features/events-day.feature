Feature:
  In order to be able to know all events of the current day
  As an app user
  I want to be able to see a all events of a day

  @Minimal
  Scenario: Show events without using cache
    Given I have the app open.
    And I have an internet connection.
    And I already provided information about the class calendar.
    When I navigate to the "Events" screen.
    And The information isn't available in the "calendar" cache.
    Then I should see the events.
    And The information is saved in the "calendar" cache.

  @Offline
  Scenario: Show events with using cache
    Given I have the app open.
    And I already provided information about the class calendar.
    When I navigate to the "Events" screen.
    And The information is available in the "calendar" cache.
    Then I should see the events.

  Scenario: Can't show next events because missing calendar information
    Given I have the app open.
    And I haven't set up the class calendar.
    Then I should see an error message.

  @Error
  Scenario: Can't show next events
    Given I have the app open.
    And The information isn't available in the "calendar" cache.
    And I haven't already provided information about the class calendar.
    Then I should see an message asking for information.

  