Feature: 
  In order to be able to reach the next transportation (e.g. urban railway)
  As an app user
  I want to be able to see a short overview about the next few possibilities

  @BasicFlow @Test
  Scenario: Show next transportation without using cache
    # Given The information isn't available in the "transportation" cache.
    # And I have an internet connection.
    Then I touch the "Übersicht" text
    Then I should see text containing "S-Bahn"
    # And The information is saved in the "transportation" cache.
    And I take a screenshot

  @Offline
  Scenario: Show next transportation with using cache
    Given The information is available in the "transportation" cache.
    Then I should see the next transportation.
    And I take a screenshot.

  @Error
  Scenario: Can't show next transportation
    Given The information isn't available in the "transportation" cache.
    And I don't have an internet connection.
    Then I should see an error message.
    And I take a screenshot.

  