Feature: 
  In order to be able plan which transportation to use at the end of the current day
  As an app user
  I want to be able to select a time frame and see all possibilities afterwards

  @Minimal
  Scenario: Show transportation without using cache
    Given I have the app open.
    And I have an internet connection.
    When I navigate to the "Transportation" screen.
    And I select a time frame.
    And The information isn't available in the "transportation" cache.
    Then I should see the next transportation.
    And The information is saved in the "transportation" cache.

  @Offline
  Scenario: Show transportation with using cache
    Given I have the app open.
    When I navigate to the "Transportation" screen.
    And I select a time frame.
    And The information is available in the "transportation" cache.
    Then I should see the next transportation.

  @Error
  Scenario: Can't show transportation
    Given I have the app open.
    When I navigate to the "Transportation" screen.
    And The information isn't available in the "transportation" cache.
    And I don't have an internet connection.
    Then I should see an error message.

  