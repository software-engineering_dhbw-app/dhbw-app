Feature:
  In order to decide if I want to eat at the cafeteria today
  As an app user
  I want to be able to see a meal plan for current day in a short overview

  @BasicFlow
  Scenario: Show meals for current day without cache
    Given The information isn't available in the "cafeteria" cache.
    And I have an internet connection.
    Then I should see meals for the current day.
    And The information is saved in the "cafeteria" cache.
    And I take a screenshot.

  @Offline
  Scenario: Show meals for current day with cache
    Given The information is available in the "cafeteria" cache.
    Then I should see meals for the current day.
    And I take a screenshot.

  @Error
  Scenario: Can't show meals for the current day
    Given The information isn't available in the "cafeteria" cache.
    And I don't have an internet connection.
    Then I should see an message asking for information.
    And I take a screenshot.

  