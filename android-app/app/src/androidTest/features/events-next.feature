Feature:
  In order to be able to reach the next event in the class schedule (i.e. lecture, exam)
  As an app user
  I want to be able to see a short overview about the next events

  Scenario:
    When I touch the "Stundenplan" text
    Then I see "Theoretische Informatik"
    And I take a screenshot

  @Minimal
  Scenario: Show next events without using cache
    Given I already provided information about the class calendar.
    And The information isn't available in the "calendar" cache.
    And I have an internet connection.
    When I touch the "Stundenplan" text
    Then I see "Theoretische Informatik"
    And I take a screenshot
    And The information is saved in the "calendar" cache.

  @Offline
  Scenario: Show next events with using cache
    Given I have the app open.
    And I already provided information about the class calendar.
    And The information is available in the "calendar" cache.
    Then I should see the next events.

  Scenario: Can't next events because missing calendar information
    Given I have the app open.
    And I haven't already provided information about the class calendar.
    Then I should see an message asking for information.

  @Error
  Scenario: Can't show next events
    Given I have the app open.
    And The information isn't available in the "calendar" cache.
    And I don't have an internet connection.
    Then I should see an error message.

  