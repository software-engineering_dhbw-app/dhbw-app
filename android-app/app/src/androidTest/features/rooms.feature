Feature:
  In order to find my way to a room
  As an app user
  I want to be able to search for a room and see its location on a map

  @Minimal
  Scenario: Show room without using cache
    Given I have the app open.
    And I have an internet connection.
    When I navigate to the "Room" screen.
    And I search for a room.
    And The information isn't available in the "room" cache.
    Then The information is saved in the "news" cache.
    And The location of the room is shown.

  @Offline
  Scenario: Show room with using cache
    Given I have the app open.
    When I navigate to the "Room" screen.
    And I search for a room.
    And The information is available in the "room" cache.
    Then The location of the room is shown.

  @Error @Offline
  Scenario: Can't load room information
    Given I have the app open.
    And I don't have an internet connection.
    When I navigate to the "Room" screen.
    And I search for a room.
    And The information isn't available in the "news" cache.
    Then I should see an error message.

  