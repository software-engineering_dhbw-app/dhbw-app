package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CalendarFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    protected Activity mActivity;

    private int section_number;

    private LayoutInflater inflater;
    private View rootView;


    private InfiniteViewPager mViewPager;

    public static CalendarFragment newInstance(int sectionNumber) {
        CalendarFragment fragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        this.rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        mViewPager = (InfiniteViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(new MyInfinitePagerAdapter(0));
        mViewPager.setPageMargin(20);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        mActivity = activity;
    }

    private class MyInfinitePagerAdapter extends InfinitePagerAdapter<Integer> {

        private LessonsDayListViewAdapter lessonsListViewAdapter;
        private ArrayList<DataObjects.LessonDay> weekLessons;
        private ListView cardsListView;

        private View timetableProgress;
        private ArrayList<DataObjects.Lesson> lessons;
        private int mShortAnimationDuration;

        public MyInfinitePagerAdapter(final Integer initValue) {
            super(initValue);
        }

        @Override
        public ViewGroup instantiateItem(Integer indicator) {
            final LinearLayout layout = (LinearLayout) ((LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE))
                    .inflate(R
                            .layout
                            .fragment_calendar_adapter, null);

            lessons = ((ApplicationT) mActivity.getApplication()).getLessonData(); // returns null if not cached or outdated
            if(lessons == null) {
                new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/calendar/TINF13B2/week/" + ContentHelper.getDayWeek(0), "0"});
            }

            this.timetableProgress = (android.support.v7.widget.CardView) layout.findViewById(R.id.loading_card);
            mShortAnimationDuration = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            this.cardsListView = (ListView) layout.findViewById(R.id.cards_list);

            if (weekLessons == null) {
                weekLessons = new ArrayList<DataObjects.LessonDay>();
            }

            weekLessons = (ArrayList<DataObjects.LessonDay>) ContentHelper.getWeekLessons(lessons, indicator);
            if(weekLessons != null) {
                lessonsListViewAdapter = new LessonsDayListViewAdapter(mActivity.getApplicationContext(), weekLessons);
                cardsListView.setAdapter(lessonsListViewAdapter);
                lessonsListViewAdapter.notifyDataSetChanged();
            } else {
                new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/calendar/TINF13B2/week/" + ContentHelper.getDayWeek(indicator), indicator.toString()});
            }

            layout.setTag(indicator);
            return layout;
        }

        @Override
        public Integer getNextIndicator() {
            return getCurrentIndicator() + 1;
        }

        @Override
        public Integer getPreviousIndicator() {
            return getCurrentIndicator() - 1;
        }

        @Override
        public String getStringRepresentation(final Integer currentIndicator) {
            return String.valueOf(currentIndicator);
        }

        @Override
        public Integer convertToIndicator(final String representation) {
            return Integer.valueOf(representation);
        }

        /* JSON Task */
        class JSONDataTask extends AsyncTask<String, Void, String> {

            private String type;
            private String url;

            protected String doInBackground(String... urls) {

                String jsonString = NetworkUtility.getJSONData(urls[0]);
                this.type = urls[1];
                this.url = urls[0];

                return jsonString;
            }

            protected void onPostExecute(String jsonString) {
                try {
                    if (jsonString == null || jsonString.equals("dataError")) {
                        ViewUtility.switchViews(timetableProgress.findViewById(R.id.progress_view), R.layout.card_error, inflater);
                        ViewUtility.changeViews(cardsListView, timetableProgress, mShortAnimationDuration);
                        return;
                    }

                    JSONArray jArray = new JSONArray(jsonString);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        String lesson_category = oneObject.getString("category");
                        String lesson_person = oneObject.getString("person");
                        String lesson_locatiom = oneObject.getString("location");
                        String lesson_title = oneObject.getString("title");
                        String lesson_startTime = oneObject.getString("startTime");
                        String lesson_endTime = oneObject.getString("endTime");

                        DataObjects.Lesson entry = new DataObjects.Lesson(lesson_category, lesson_title, lesson_person, lesson_locatiom, lesson_startTime, lesson_endTime, ViewUtility.stringToHex(lesson_title + lesson_person));
                        lessons.add(entry);
                    }

                    ((ApplicationT) mActivity.getApplication()).setLessonData(lessons);

                    weekLessons = (ArrayList<DataObjects.LessonDay>) ContentHelper.getWeekLessons(lessons, Integer.parseInt(this.type));
                    lessonsListViewAdapter = new LessonsDayListViewAdapter(mActivity.getApplicationContext(), weekLessons);
                    if(weekLessons != null){
                        cardsListView.setAdapter(lessonsListViewAdapter);
                        lessonsListViewAdapter.notifyDataSetChanged();
                    } else {
                        ViewUtility.switchViews(timetableProgress.findViewById(R.id.progress_view), R.layout.card_no_information, inflater);
                        ViewUtility.changeViews(cardsListView, timetableProgress, mShortAnimationDuration);
                    }
                } catch (JSONException e) {
                    Log.e("DHWB", "JSON: " + jsonString);
                    Log.e("DHBW", e.getMessage(), e);
                }
            }
        }
    }
}
