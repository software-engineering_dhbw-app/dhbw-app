package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import java.io.*;
import java.util.ArrayList;

public class CacheHelper {

    // TODO delete old data function for app start

    /*
     * Global get/save Object Functions
     */
    @SuppressWarnings("rawtypes")
    public static boolean saveObject(ArrayList obj, String file, File cacheDir) {
        final File suspend_f = new File(cacheDir, file);

        FileOutputStream fos  = null;
        ObjectOutputStream oos  = null;
        boolean            keep = true;

        try {
            fos = new FileOutputStream(suspend_f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
        } catch (Exception e) {
            keep = false;
        } finally {
            try {
                if (oos != null)   oos.close();
                if (fos != null)   fos.close();
                if (keep == false) suspend_f.delete();
            } catch (Exception e) { /* do nothing */
            }
        }

        return keep;
    }

    @SuppressWarnings("rawtypes")
    public static ArrayList getObject(String file, File cacheDir) {
        Boolean found = false;
        ArrayList simpleClass= null;
        FileInputStream fis = null;
        ObjectInputStream is = null;
        File suspend_f = new File(cacheDir, file);

        if(suspend_f.exists()) {
            found  = true;
        } else {
            found = false;
        }

        if(found){
            try {
                fis = new FileInputStream(suspend_f);
                is = new ObjectInputStream(fis);
                simpleClass = (ArrayList) is.readObject();
            } catch(Exception e) {
            } finally {
                try {
                    if (fis != null)   fis.close();
                    if (is != null)   is.close();
                } catch (Exception e) { }
            }
        } else {
            //file not found
            return null;
        }

        return simpleClass;
    }
}
