package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.*;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DHBWNewsFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_NEWS_ENTRY = "news_entry";
    protected Activity mActivity;

    private int section_number;
    private DataObjects.DHBWNews news_entry;

    private LayoutInflater inflater;
    private SwipeRefreshLayout swipeLayout;
    private View rootView;

    private android.support.v7.widget.CardView loadingCard;
    private LinearLayout cardsList;

    private ArrayList<DataObjects.DHBWNews> dhbwNews;

    private int mShortAnimationDuration;

    public static DHBWNewsFragment newInstance(int sectionNumber) {
        DHBWNewsFragment fragment = new DHBWNewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public static DHBWNewsFragment newInstance(int sectionNumber, DataObjects.DHBWNews newsEntry) {
        DHBWNewsFragment fragment = new DHBWNewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_NEWS_ENTRY, newsEntry);
        fragment.setArguments(args);
        return fragment;
    }

    public DHBWNewsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        if (this.section_number == 5){
            this.rootView = inflater.inflate(R.layout.fragment_cards_list, container, false);

            this.loadingCard = (android.support.v7.widget.CardView) rootView.findViewById(R.id.loading_card);
            this.cardsList = (LinearLayout) rootView.findViewById(R.id.cards_list);

            showCache();
        } else if (this.section_number == 8){
            this.rootView = inflater.inflate(R.layout.fragment_dhbw_news, container, false);

            this.news_entry = (DataObjects.DHBWNews) args.getSerializable(ARG_NEWS_ENTRY);

            TextView titleView = (TextView) rootView.findViewById(R.id.news_title);
            TextView descriptionView = (TextView) rootView.findViewById(R.id.news_description);
            WebView news_content = (WebView) rootView.findViewById(R.id.news_content);
            news_content.setWebViewClient(new CustomWebViewClient(mActivity));

            titleView.setText(news_entry.title);
            descriptionView.setText(news_entry.description);
            String postText =  "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /></head><body style='background-color: #fafafa!important; font-family: sans-serif-light;'>" + news_entry.content + "</body>";
            news_content.loadDataWithBaseURL("fake://fake.de", postText, "text/html", "UTF-8", null);
        }

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        if(this.section_number == 5) {
            swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadNewsData(false);
                }
            });

            loadNewsData(true);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        mActivity = activity;
    }

    /* JSON Task */
    class JSONDataTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {

            String jsonString = NetworkUtility.getJSONData(urls[0]);

            return jsonString;
        }

        protected void onPostExecute(String jsonString) {
            dhbwNews.clear();

            try {
                if (jsonString == null || jsonString.equals("dataError")) {
                    ViewUtility.switchViews(loadingCard.findViewById(R.id.progress_view), R.layout.card_error, inflater);
                    ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
                    return;
                }
                JSONArray jArray = new JSONArray(jsonString);
                for (int i=0; i < jArray.length(); i++) {
                    JSONObject oneObject = jArray.getJSONObject(i);

                    String dhbwNews_title = oneObject.getString("title");
                    String dhbwNews_description = oneObject.getString("description");
                    String dhbwNews_content = oneObject.getString("content");
                    String dhbwNews_published = oneObject.getString("published");
                    String dhbwNews_link = oneObject.getString("link");

                    DataObjects.DHBWNews entry = new DataObjects.DHBWNews(dhbwNews_content, dhbwNews_published, dhbwNews_link, dhbwNews_description, dhbwNews_title);
                    dhbwNews.add(entry);
                }

                ((ApplicationT) mActivity.getApplication()).setDHBWNewsData(dhbwNews);
                fillDHBWNews();

            } catch (JSONException e) {
                Log.e("DHBW", e.getMessage(), e);
            }
        }
    }
    public void fillDHBWNews(){

        if (((LinearLayout) cardsList).getChildCount() > 0) {
            ((LinearLayout) cardsList).removeAllViews();
        }

        // DHBW News
        for (int i = 0; i < dhbwNews.size(); i++) {
            View view = inflater.inflate(R.layout.dhbw_news_overview, null);

            final DataObjects.DHBWNews entry = dhbwNews.get(i);

            TextView titleView = (TextView) view.findViewById(R.id.news_title);
            TextView descriptionView = (TextView) view.findViewById(R.id.news_description);

            titleView.setText(entry.title);
            descriptionView.setText(entry.description);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, DHBWNewsFragment.newInstance(8, entry))
                            .commit();
                }
            });
            cardsList.addView(view);
        }
        ViewUtility.changeViews(loadingCard, cardsList, mShortAnimationDuration);
    }


    public void loadNewsData(Boolean start) {
        if (this.section_number == 5){
            if(!start) {
                ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
            }

            new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/dhbw-news/"});
        }

        swipeLayout.setRefreshing(false);
    }

    private void showCache() {
        dhbwNews = ((ApplicationT) mActivity.getApplication()).getDHBWNewsData(); // returns null if not cached or outdated
        if(dhbwNews != null) {
            fillDHBWNews();
        } else {
            dhbwNews = new ArrayList<DataObjects.DHBWNews>();
            ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
        }
    }
}