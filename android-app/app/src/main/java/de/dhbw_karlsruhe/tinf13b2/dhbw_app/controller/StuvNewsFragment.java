package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StuvNewsFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_NEWS_ENTRY = "news_entry";
    protected Activity mActivity;

    private int section_number = 0;
    private DataObjects.StuvNews news_entry;

    private LayoutInflater inflater;
    private SwipeRefreshLayout swipeLayout;
    private View rootView;

    private android.support.v7.widget.CardView loadingCard;
    private LinearLayout cardsList;

    private ArrayList<DataObjects.StuvNews> stuvNews;

    private int mShortAnimationDuration;

    public static StuvNewsFragment newInstance(int sectionNumber) {
        StuvNewsFragment fragment = new StuvNewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public static StuvNewsFragment newInstance(int sectionNumber, DataObjects.StuvNews newsEntry) {
        StuvNewsFragment fragment = new StuvNewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_NEWS_ENTRY, newsEntry);
        fragment.setArguments(args);
        return fragment;
    }

    public StuvNewsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        if (this.section_number == 6) {
            this.rootView = inflater.inflate(R.layout.fragment_cards_list, container, false);

            this.loadingCard = (android.support.v7.widget.CardView) rootView.findViewById(R.id.loading_card);
            this.cardsList = (LinearLayout) rootView.findViewById(R.id.cards_list);

            showCache();
        } else if (this.section_number == 9) {
            this.rootView = inflater.inflate(R.layout.fragment_dhbw_news, container, false);

            this.news_entry = (DataObjects.StuvNews) args.getSerializable(ARG_NEWS_ENTRY);

            TextView titleView = (TextView) rootView.findViewById(R.id.news_title);
            TextView descriptionView = (TextView) rootView.findViewById(R.id.news_description);
            WebView news_content = (WebView) rootView.findViewById(R.id.news_content);
            news_content.setWebViewClient(new CustomWebViewClient(mActivity));

            titleView.setText(news_entry.title);
            descriptionView.setVisibility(View.GONE); // as StuV has no description in their news
            String postText = "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /></head><body style='background-color: #fafafa!important; font-family: sans-serif-light;'>" + news_entry.content + "</body>";
            news_content.loadDataWithBaseURL("fake://fake.de", postText, "text/html", "UTF-8", null);
        }

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        if (this.section_number == 6) {
            swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadStuvNewsData(false);
                }
            });

            loadStuvNewsData(true);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

       mActivity = activity;
    }

    /* JSON Task */
    class JSONDataTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {

            String jsonString = NetworkUtility.getJSONData(urls[0]);

            return jsonString;
        }

        protected void onPostExecute(String jsonString) {
            stuvNews.clear();

            try {
                if (jsonString == null || jsonString.equals("dataError")) {
                    ViewUtility.switchViews(loadingCard.findViewById(R.id.progress_view), R.layout.card_error, inflater);
                    ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
                    return;
                }
                JSONArray jArray = new JSONArray(jsonString);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject oneObject = jArray.getJSONObject(i);

                    String stuvNews_title = oneObject.getString("title");
                    String stuvNews_content = oneObject.getString("content");
                    String stuvNews_published = oneObject.getString("published");
                    String stuvNews_link = oneObject.getString("link");

                    DataObjects.StuvNews entry = new DataObjects.StuvNews(stuvNews_content, stuvNews_published, stuvNews_link, stuvNews_title);
                    stuvNews.add(entry);
                }

                ((ApplicationT) mActivity.getApplication()).setStuvNewsData(stuvNews);
                fillStuvNews();

            } catch (JSONException e) {
                Log.e("DHBW", e.getMessage(), e);
            }
        }
    }

    public void fillStuvNews() {

        if (((LinearLayout) cardsList).getChildCount() > 0) {
            ((LinearLayout) cardsList).removeAllViews();
        }

        // Stuv News
        for (int i = 0; i < stuvNews.size(); i++) {
            View view = inflater.inflate(R.layout.dhbw_news_overview, null);

            final DataObjects.StuvNews entry = stuvNews.get(i);

            TextView titleView = (TextView) view.findViewById(R.id.news_title);
            TextView descriptionView = (TextView) view.findViewById(R.id.news_description);

            titleView.setText(entry.title);
            descriptionView.setVisibility(View.GONE); // as StuV has no description in their News

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, StuvNewsFragment.newInstance(9, entry))
                            .commit();
                }
            });
            cardsList.addView(view);
        }
        ViewUtility.changeViews(loadingCard, cardsList, mShortAnimationDuration);
    }


    public void loadStuvNewsData(Boolean start) {
        if (this.section_number == 6) {
            if(!start) {
                ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
            }

            new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/stuv-news/"});
        }

        swipeLayout.setRefreshing(false);
    }

    private void showCache() {
        stuvNews = ((ApplicationT) mActivity.getApplication()).getStuvNewsData(); // returns null if not cached or outdated
        if (stuvNews != null) {
            fillStuvNews();
        } else {
            stuvNews = new ArrayList<DataObjects.StuvNews>();
            ViewUtility.changeViews(cardsList, loadingCard, mShortAnimationDuration);
        }
    }
}
