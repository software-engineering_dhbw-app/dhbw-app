package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.daimajia.swipe.util.Attributes;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ContactsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    protected Activity mActivity;

    private int section_number;

    private LayoutInflater inflater;
    private SwipeRefreshLayout swipeLayout;
    private View rootView;

    private EmployeesListViewAdapter employeesAdapter;
    private ArrayList<DataObjects.Employee> employees;
    private ListView cardsListView;
    private CardView searchCard;

    private android.support.v7.widget.CardView loadingCard;

    private int mShortAnimationDuration;

    public static ContactsFragment newInstance(int sectionNumber) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ContactsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        this.rootView = inflater.inflate(R.layout.fragment_contact_list, container, false);

        this.loadingCard = (android.support.v7.widget.CardView) rootView.findViewById(R.id.loading_card);
        this.cardsListView = (ListView) rootView.findViewById(R.id.cards_list);
        this.searchCard = (android.support.v7.widget.CardView) rootView.findViewById(R.id.search_card);
        this.cardsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (cardsListView == null || cardsListView.getChildCount() == 0) ?
                                0 : cardsListView.getChildAt(0).getTop();
                rootView.findViewById(R.id.swipe_container).setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        this.cardsListView.setTextFilterEnabled(true);
        EditText search = (EditText) rootView.findViewById(R.id.contacts_search);

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                employeesAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        showCache();

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(false);
            }
        });

        loadData(true);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        mActivity = activity;
    }

    /* JSON Task */
    class JSONDataTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {

            String jsonString = NetworkUtility.getJSONData(urls[0]);

            return jsonString;
        }

        protected void onPostExecute(String jsonString) {
            employees.clear();

            try {
                if (jsonString == null || jsonString.equals("dataError")) {
                    ViewUtility.switchViews(loadingCard.findViewById(R.id.progress_view), R.layout.card_error, inflater);
                    ViewUtility.changeViews(cardsListView, loadingCard, mShortAnimationDuration);
                    return;
                }

                JSONArray jArray = new JSONArray(jsonString);
                for (int i=0; i < jArray.length(); i++) {
                    JSONObject oneObject = jArray.getJSONObject(i);

                    String contacts_name = oneObject.getString("name");
                    String contacts_function = oneObject.getString("function");
                    String contacts_telephone = oneObject.getString("telephone");
                    String contacts_room = oneObject.getString("room");

                    DataObjects.Employee entry = new DataObjects.Employee(contacts_name, contacts_function, contacts_telephone, contacts_room);
                    employees.add(entry);
                }

                ((ApplicationT) mActivity.getApplication()).setContactData(employees);
                fillContacts();

            } catch (JSONException e) {
                Log.e("DHBW", e.getMessage(), e);
            }
        }
    }


    public void fillContacts(){

        employeesAdapter = new EmployeesListViewAdapter(mActivity.getApplicationContext(), employees);
        cardsListView.setAdapter(employeesAdapter);
        employeesAdapter.setMode(Attributes.Mode.Single);
        employeesAdapter.notifyDataSetChanged();
        ViewUtility.changeViews(loadingCard, cardsListView, mShortAnimationDuration);
        ViewUtility.showView(searchCard, mShortAnimationDuration);
    }

    public void loadData(Boolean start) {
        if(!start) {
            ViewUtility.changeViews(cardsListView, loadingCard, mShortAnimationDuration);
            ViewUtility.hideView(searchCard, mShortAnimationDuration);
        }

        new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/contact/", "contacts"});

        swipeLayout.setRefreshing(false);
    }

    private void showCache() {
        // train
        employees = ((ApplicationT) mActivity.getApplication()).getContactData(); // returns null if not cached or outdated
        if(employees != null) {
            fillContacts();
        } else {
            employees = new ArrayList<DataObjects.Employee>();
            ViewUtility.changeViews(cardsListView, loadingCard, mShortAnimationDuration);
            ViewUtility.hideView(searchCard, mShortAnimationDuration);
        }
    }
}