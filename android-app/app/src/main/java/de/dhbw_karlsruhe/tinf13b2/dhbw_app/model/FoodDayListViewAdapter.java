package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;

import java.util.ArrayList;

public class FoodDayListViewAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<DataObjects.FoodDay> food;

    public FoodDayListViewAdapter(Context mContext, ArrayList<DataObjects.FoodDay> food) {
        this.mContext = mContext;
        this.food = food;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.list_calendar_main, null);

        final DataObjects.FoodDay entry = food.get(position);

        TextView dateView = (TextView) view.findViewById(R.id.date);
        LinearLayout listView = (LinearLayout) view.findViewById(R.id.cards_list);

        dateView.setText(ViewUtility.getDateStr(entry.date));

        for (DataObjects.Food singleFood : entry.meals) {
            View vi = LayoutInflater.from(mContext).inflate(R.layout.cafeteria_main, null);

            TextView nameView = (TextView) vi.findViewById(R.id.meal_name);
            TextView priceView = (TextView) vi.findViewById(R.id.meal_price);

            nameView.setText(singleFood.name);
            priceView.setText(singleFood.price);

            listView.addView(vi);
        }

        return view;
    }

    @Override
    public int getCount() {
        return food.size();
    }

    @Override
    public Object getItem(int position) {
        return food.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
