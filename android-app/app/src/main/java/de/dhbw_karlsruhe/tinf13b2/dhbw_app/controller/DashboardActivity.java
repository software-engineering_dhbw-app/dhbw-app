package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import com.crashlytics.android.Crashlytics;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.NoSSLv3Factory;
import io.fabric.sdk.android.Fabric;

import javax.net.ssl.HttpsURLConnection;


public class DashboardActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private int sectionNumber = 0;

    private Toolbar toolbar;

    static {
        HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());
        java.lang.System.setProperty("https.protocols", "TLSv1");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        String[] menu_titles = {
                getString(R.string.section_main),
                getString(R.string.section_events),
                getString(R.string.section_cafeteria),
                //getString(R.string.section_transportation),
                getString(R.string.section_employee),
                getString(R.string.section_dhbw_news),
                getString(R.string.section_stuv_news)};

        int[] menu_icons = {
                R.drawable.ic_home,
                R.drawable.ic_event,
                R.drawable.ic_dashboard,
                //R.drawable.ic_home,
                R.drawable.ic_perm_identity,
                R.drawable.ic_announcement,
                R.drawable.ic_announcement,
        };

        mNavigationDrawerFragment.setMenu(menu_titles, menu_icons);
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                this,
                "TINF13B2");

        //throw new RuntimeException("This is a crash");
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getFragmentManager();
        Log.i("DHWB", "pos: " + position);

        switch(position) {
            case 1:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, CalendarFragment.newInstance(2))
                        .commit();
                break;
            case 2:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FoodsFragment.newInstance(3))
                        .commit();
                break;
            case 3:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ContactsFragment.newInstance(4))
                        .commit();
                break;
            case 4:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, DHBWNewsFragment.newInstance(5))
                        .commit();
                break;
            case 5:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, StuvNewsFragment.newInstance(6))
                        .commit();
                break;
            default:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, DashboardFragment.newInstance(position + 1))
                        .commit();
                break;
        }
    }

    public void onSectionAttached(int number) {
        sectionNumber = number;
        switch (number) {
            case 1:
                mTitle = getString(R.string.section_main);
                break;
            case 2:
                mTitle = getString(R.string.section_events);
                break;
            case 3:
                mTitle = getString(R.string.section_cafeteria);
                break;
            /*case 4:
                mTitle = getString(R.string.section_transportation);
                break;*/
            case 4:
                mTitle = getString(R.string.section_employee);
                break;
            case 5:
                mTitle = getString(R.string.section_dhbw_news);
                break;
            case 6:
                mTitle = getString(R.string.section_stuv_news);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.

            // we dont need a menu!
            //getMenuInflater().inflate(R.menu.dashboard, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        FragmentManager fragmentManager = getFragmentManager();
        switch (sectionNumber) {
            case 1:
                super.onBackPressed();
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            //case 6:
            case 6:
                onSectionAttached(1);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, DashboardFragment.newInstance(1))
                        .commit();
                break;
            case 9:
                onSectionAttached(6);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, StuvNewsFragment.newInstance(6))
                        .commit();
                break;
            case 8:
                onSectionAttached(5);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, DHBWNewsFragment.newInstance(5))
                        .commit();
                break;
        }

        restoreActionBar();
    }
}
