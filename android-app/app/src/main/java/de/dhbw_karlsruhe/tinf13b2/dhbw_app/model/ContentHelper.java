package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.util.Log;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ContentHelper {

    public static DataObjects.Train getCurrentTrain(ArrayList<DataObjects.Train> list) {
        DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();

        for (DataObjects.Train train : list) {
            if (parser.parseDateTime(train.time).isAfterNow()) {
                return train;
            }
        }

        return null;
    }

    public static ArrayList<DataObjects.Food> getTodaysFood(ArrayList<DataObjects.FoodDay> list) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        String currentDate = sdf.format(d);

        for (DataObjects.FoodDay day : list) {
            if(day.date.equals(currentDate)) {
                return day.meals;
            }
        }

        return null;
    }

    public static ArrayList<DataObjects.Lesson> getTodaysLessons(ArrayList<DataObjects.Lesson> list) {
        DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();
        ArrayList<DataObjects.Lesson> newList = new ArrayList<DataObjects.Lesson>();
        LocalDate today = new LocalDate();

        for (DataObjects.Lesson lesson : list) {
            LocalDate date = parser.parseLocalDate(lesson.startTime);

            if(date.isEqual(today)) {
                newList.add(lesson);
            }
        }
        //TODO: if last entry endTime is after now -> show events of next day
        //TODO: if newList == null -> +1 Day ?

        return newList;
    }

    /**
     * return all lessons in cuurrent week + indecator
     * requirements: list has to be sorted ASC
     * @param list
     * @param indecator
     * @return
     */
    public static ArrayList<DataObjects.LessonDay> getWeekLessons(ArrayList<DataObjects.Lesson> list, int indecator) {
        ArrayList<DataObjects.LessonDay> newList = new ArrayList<DataObjects.LessonDay>();

        if(list.size() > 0) {
            DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();
            LocalDate now = new LocalDate();
            now = now.plusWeeks(indecator);

            LocalDate monday = now.withDayOfWeek(DateTimeConstants.MONDAY);
            LocalDate sunday = now.withDayOfWeek(DateTimeConstants.SUNDAY);

            ArrayList<DataObjects.Lesson> lessonList = new ArrayList<DataObjects.Lesson>();

            String curDate = parser.parseDateTime(list.get(0).startTime).toString("dd.MM.yyyy");
            for (DataObjects.Lesson lesson : list) {
                LocalDate date = parser.parseLocalDate(lesson.startTime);

                if (!curDate.equals(date.toString("dd.MM.yyyy"))) {
                    if (lessonList.size() > 0) {
                        DataObjects.LessonDay entry = new DataObjects.LessonDay(curDate, lessonList);
                        newList.add(entry);
                        lessonList = new ArrayList<DataObjects.Lesson>();
                    }

                    curDate = date.toString("dd.MM.yyyy");
                }

                if (date.isAfter(monday.minusDays(1)) & date.isBefore(sunday.plusDays(1))) {
                    lessonList.add(lesson);
                }
            }

            // still entries ?
            if (lessonList.size() > 0) {
                DataObjects.LessonDay entry = new DataObjects.LessonDay(curDate, lessonList);
                newList.add(entry);
            }
        }

        if (newList.size() == 0){
            return null;
        }
        return newList;
    }

    /**
     * return all meals in cuurrent week + indecator
     * requirements: list has to be sorted ASC
     * @param list
     * @param indecator
     * @return
     */
    public static ArrayList<DataObjects.FoodDay> getWeekFood(ArrayList<DataObjects.FoodDay> list, int indecator) {
        ArrayList<DataObjects.FoodDay> newList = new ArrayList<DataObjects.FoodDay>();

        if(list.size() > 0) {
            DateTimeFormatter parser = DateTimeFormat.forPattern("yyyy-MM-dd");
            LocalDate now = new LocalDate();
            now = now.plusWeeks(indecator);

            LocalDate monday = now.withDayOfWeek(DateTimeConstants.MONDAY);
            LocalDate sunday = now.withDayOfWeek(DateTimeConstants.SUNDAY);

            for (DataObjects.FoodDay food : list) {
                LocalDate date = parser.parseLocalDate(food.date);

                if (date.isAfter(monday.minusDays(1)) & date.isBefore(sunday.plusDays(1))) {
                    if(food.meals.size() > 0) {
                        newList.add(food);
                    }
                }
            }
        }

        if (newList.size() == 0){
            return null;
        }
        return newList;
    }

    public static String getDayWeek(int indecator) {
        LocalDate now = new LocalDate();
        now = now.plusWeeks(indecator);
        return now.toString("yyyy-MM-dd");
    }
}
