package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;

import java.util.ArrayList;

public class EmployeesListViewAdapter extends BaseSwipeAdapter {

    private ItemFilter mFilter = new ItemFilter();
    private Context mContext;
    private ArrayList<DataObjects.Employee> originalEmployees;
    private ArrayList<DataObjects.Employee> filteredEmployees;

    public EmployeesListViewAdapter(Context mContext, ArrayList<DataObjects.Employee> employees) {
        this.mContext = mContext;
        this.originalEmployees = employees;
        this.filteredEmployees = employees;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_contact_persons, null);
        SwipeLayout swipeLayout = (SwipeLayout) v.findViewById(getSwipeLayoutResourceId(position));

        v.findViewById(R.id.contact_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + filteredEmployees.get(position).telephone));
                callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.getApplicationContext().startActivity(callIntent);
            }
        });

        v.findViewById(R.id.contact_copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(mContext.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(filteredEmployees.get(position).name, filteredEmployees.get(position).telephone);
                clipboard.setPrimaryClip(clip);

                Toast.makeText(mContext, filteredEmployees.get(position).telephone + " " + mContext.getString(R.string.copied), Toast.LENGTH_SHORT).show();
            }
        });

        // TODO: Version 2 we will integrate room search function
        TextView room_view = (TextView) v.findViewById(R.id.room_search_text);
        room_view.setText(filteredEmployees.get(position).room);

        return v;
    }

    @Override
    public void fillValues(int position, View convertView) {
        final DataObjects.Employee entry = filteredEmployees.get(position);

        TextView nameView = (TextView) convertView.findViewById(R.id.contact_name);
        TextView descriptionView = (TextView) convertView.findViewById(R.id.contact_description);
        TextView roomView = (TextView) convertView.findViewById(R.id.contact_room);

        nameView.setText(entry.name);
        descriptionView.setText(entry.function);
        roomView.setText(entry.room);

        TextView contact_image = (TextView) convertView.findViewById(R.id.contact_image);
        String[] parts = entry.name.split(",");
        contact_image.setText(parts[0].trim().substring(0, 1) + parts[1].trim().substring(0, 1));

        int color;
        try {
            color = Color.parseColor(ViewUtility.stringToHex(entry.name));
        } catch (java.lang.IllegalArgumentException e) {
            color = Color.parseColor("#7D8990");
        }
        GradientDrawable bgShape = (GradientDrawable) contact_image.getBackground();
        bgShape.setColor(color);
    }

    @Override
    public int getCount() {
        return filteredEmployees.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredEmployees.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<DataObjects.Employee> list = originalEmployees;

            int count = list.size();
            final ArrayList<DataObjects.Employee> nlist = new ArrayList<DataObjects.Employee>(count);

            String filterableString;
            DataObjects.Employee filterableEmployee;

            for (int i = 0; i < count; i++) {
                filterableEmployee = list.get(i);
                filterableString = filterableEmployee.name;
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(filterableEmployee);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredEmployees = (ArrayList<DataObjects.Employee>) results.values;
            notifyDataSetChanged();
        }
    }
}
