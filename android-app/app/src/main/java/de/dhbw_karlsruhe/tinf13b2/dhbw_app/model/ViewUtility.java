package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ViewUtility {
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static String formatPrice(String price){
        String price_result = "";
        try {
            Float number = Float.parseFloat(price);
            price_result = String.format("%10.2f", number) + " €";
        } catch (java.lang.NumberFormatException e){} // if cafeteria api gives null

        return price_result;
    }

    public static Integer getTimeSpan(String time){
        if(time == "" || time == null){
            return 0;
        }
        DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();
        Date d;
        d = parser.parseDateTime(time).toDate();

        Calendar thatDay = Calendar.getInstance();
        thatDay.set(Calendar.HOUR_OF_DAY, d.getHours());
        thatDay.set(Calendar.MINUTE, d.getMinutes());

        Calendar today = Calendar.getInstance();

        long diff = thatDay.getTimeInMillis() - today.getTimeInMillis();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

        return Math.round(minutes);
    }

    public static String getTimeStr (String time) {
        DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();
        Date date;
        date = parser.parseDateTime(time).toDate();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        return sdf.format(date);
    }

    public static String getDateStr (String givenDate) {
        DateTimeFormatter parser = DateTimeFormat.forPattern("yyyy-MM-dd");
        Date date;
        date = parser.parseDateTime(givenDate).toDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        return sdf.format(date);
    }

    public static String stringToHex(String text){
        int stringHash = text.hashCode() % 0xFFFFFF;

        return String.format("#%X", stringHash);
    }

    public static void showView (View _show, int mShortAnimationDuration){
        final View show = _show;

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        show.setAlpha(0f);
        show.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        show.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);
    }

    public static void hideView (View _hide, int mShortAnimationDuration){
        final View hide = _hide;

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        hide.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hide.setVisibility(View.GONE);
                    }
                });
    }

    public static void switchViews (View C, int newLayout, LayoutInflater inflater) {
        if (C != null) {
            ViewGroup parent = (ViewGroup) C.getParent();
            int index = parent.indexOfChild(C);
            parent.removeView(C);

            C = inflater.inflate(newLayout, parent, false);
            parent.addView(C, index);
        } else {
            Log.e("DHBW", "View C is null!");
        }
    }
    public static void changeViews (View _shown, View hidden, int mShortAnimationDuration){

        final View shown = _shown;

        // FOR LOLLIPOP !!!
        /*
        // get the center for the clipping circle
        int cxHidden = (hidden.getLeft() + hidden.getRight()) / 2;
        int cyHidden = (hidden.getTop() + hidden.getBottom()) / 2;

        int cxShown = (shown.getLeft() + shown.getRight()) / 2;
        int cyShown = (shown.getTop() + shown.getBottom()) / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(hidden.getWidth(), hidden.getHeight());

        int initialRadius = shown.getWidth();

        // create the animator for this view (the start radius is zero)
        Animator animHidden =
                ViewAnimationUtils.createCircularReveal(hidden, cxHidden, cyHidden, 0, finalRadius);

        Animator animShown =
                ViewAnimationUtils.createCircularReveal(shown, cxShown, cyShown, initialRadius, 0);

        // make the view visible and start the animation
        hidden.setVisibility(View.VISIBLE);
        animHidden.start();


        // make the view invisible when the animation is done
        animShown.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                shown.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        animShown.start();*/

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        //hidden.setAlpha(0f);
        hidden.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        /*hidden.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);*/

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        shown.setVisibility(View.GONE);
        /*shown.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        shown.setVisibility(View.GONE);
                    }
                });*/
    }
}
