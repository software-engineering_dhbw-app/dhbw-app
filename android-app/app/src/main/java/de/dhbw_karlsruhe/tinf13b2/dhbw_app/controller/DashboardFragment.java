package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    protected Activity mActivity;

    private int section_number;

    private LayoutInflater inflater;
    private SwipeRefreshLayout swipeLayout;
    private View rootView;

    /* Dashboard */
    private View trainProgress;
    private View trainView;
    private View trainError;
    private View timetableProgress;
    private View timetableView;
    private View timetableError;
    private View cafeteriaProgress;
    private View cafeteriaView;
    private View cafeteriaError;

    private ArrayList<DataObjects.FoodDay> meals;
    private ArrayList<DataObjects.Lesson> lessons;
    private ArrayList<DataObjects.Train> trains;
    private LinearLayout mealsList;
    private LinearLayout lessonsList;

    private int mShortAnimationDuration;

    public static DashboardFragment newInstance(int sectionNumber) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        this.rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        this.trainProgress = rootView.findViewById(R.id.train_view);
        this.trainView = rootView.findViewById(R.id.train_main);
        this.trainError = rootView.findViewById(R.id.train_error);

        this.cafeteriaProgress = rootView.findViewById(R.id.cafeteria_view);
        this.cafeteriaView = rootView.findViewById(R.id.cafeteria_list);
        this.cafeteriaError = rootView.findViewById(R.id.cafeteria_error);

        this.cafeteriaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FoodsFragment.newInstance(3))
                        .commit();
            }
        });

        this.timetableProgress = rootView.findViewById(R.id.timetable_view);
        this.timetableView = rootView.findViewById(R.id.timetable_list);
        this.timetableError = rootView.findViewById(R.id.timetable_error);

        this.timetableView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, CalendarFragment.newInstance(2))
                        .commit();
            }
        });

        mealsList = (LinearLayout) rootView.findViewById(R.id.cafeteria_list);
        lessonsList = (LinearLayout) rootView.findViewById(R.id.timetable_list);

        showCache();

        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(false);
            }
        });

        loadData(true);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        mActivity = activity;
    }

    /* JSON Task */
    class JSONDataTask extends AsyncTask<String, Void, String> {

        private String type;

        protected String doInBackground(String... urls) {

            String jsonString = NetworkUtility.getJSONData(urls[0]);
            this.type = urls[1];

            return jsonString;
        }

        protected void onPostExecute(String jsonString) {
            // clear correct list
            switch (this.type) {
                case "food":
                    meals.clear();
                    break;
                case "train":
                    trains.clear();
                    break;
                case "lessons":
                    lessons.clear();
                    break;
            }

            try {
                if (jsonString == null || jsonString.equals("dataError")) {
                    switch (this.type) {
                        case "food":
                            ViewUtility.changeViews(trainView, trainError, mShortAnimationDuration);
                            break;
                        case "train":
                            ViewUtility.changeViews(cafeteriaView, cafeteriaError, mShortAnimationDuration);
                            break;
                        case "lessons":
                            ViewUtility.changeViews(timetableView, timetableError, mShortAnimationDuration);
                            break;
                    }
                    return;
                }

                JSONArray jArray = new JSONArray(jsonString);
                for (int i=0; i < jArray.length(); i++) {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    // Pulling items from the array

                    if(this.type.equals("food")) {
                        String date = oneObject.getString("date");
                        Boolean closed = oneObject.getBoolean("closed");
                        JSONArray mealsArray = oneObject.getJSONArray("meals");

                        ArrayList<DataObjects.Food> mealList = new ArrayList<DataObjects.Food>();
                        for (int j=0; j < mealsArray.length(); j++) {
                            JSONObject meal = mealsArray.getJSONObject(j);

                            String food_name = meal.getString("name");
                            String food_price = meal.getString("studentPrice");

                            DataObjects.Food entry = new DataObjects.Food(food_name, ViewUtility.formatPrice(food_price), "");
                            mealList.add(entry);
                        }

                        meals.add(new DataObjects.FoodDay(date, mealList, closed));
                    } else if (this.type.equals("train")) {
                        String train_line = oneObject.getString("line");
                        String train_destination = oneObject.getString("destination");
                        String train_time = oneObject.getString("time");
                        String train_color = oneObject.getString("lineColor");

                        DataObjects.Train entry = new DataObjects.Train(train_line, train_destination, train_time, train_color);
                        trains.add(entry);
                    } else if (this.type.equals("lessons")) {
                        String lesson_category = oneObject.getString("category");
                        String lesson_person = oneObject.getString("person");
                        String lesson_locatiom = oneObject.getString("location");
                        String lesson_title = oneObject.getString("title");
                        String lesson_startTime = oneObject.getString("startTime");
                        String lesson_endTime = oneObject.getString("endTime");

                        DataObjects.Lesson entry = new DataObjects.Lesson(lesson_category, lesson_title, lesson_person, lesson_locatiom, lesson_startTime, lesson_endTime, ViewUtility.stringToHex(lesson_title + lesson_person));
                        lessons.add(entry);
                    }
                }

                switch (this.type) {
                    case "food":
                        fillMeals();
                        ((ApplicationT) mActivity.getApplication()).setFoodData(meals);
                        break;
                    case "train":
                        fillTrains();
                        ((ApplicationT) mActivity.getApplication()).setTrainData(trains);
                        break;
                    case "lessons":
                        fillLessons();
                        ((ApplicationT) mActivity.getApplication()).setLessonData(lessons);
                        break;
                }
            } catch (JSONException e) {
                Log.e("DHBW", e.getMessage(), e);
            }
        }
    }

    public void fillMeals() {

        if (((LinearLayout) mealsList).getChildCount() > 0) {
            ((LinearLayout) mealsList).removeAllViews();
        }

        ArrayList<DataObjects.Food> mealList = ContentHelper.getTodaysFood(meals); //meals.get(i);
        if(mealList == null) {
            // TODO show no cached data avaible (check if loading data card is not shown currently)
            return;
        }

        if(mealList.size() == 0){
            TextView message = (TextView) cafeteriaError.findViewById(R.id.textView);
            message.setText(getString(R.string.no_meals));
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)message.getLayoutParams();
            params.setMargins(0, 0, 0, 5);
            message.setLayoutParams(params);
            ViewUtility.changeViews(cafeteriaView, cafeteriaError, mShortAnimationDuration);

            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, FoodsFragment.newInstance(3))
                            .commit();
                }
            });
        }

        // Meals
        for (int i = 0; i < mealList.size(); i++) {
            View view = inflater.inflate(R.layout.cafeteria_main, null);

            DataObjects.Food entry = mealList.get(i);

            TextView nameView = (TextView) view.findViewById(R.id.meal_name);
            TextView priceView = (TextView) view.findViewById(R.id.meal_price);

            nameView.setText(entry.name);
            priceView.setText(entry.price);

            mealsList.addView(view);
        }
        ViewUtility.changeViews(cafeteriaProgress, cafeteriaView, mShortAnimationDuration);
    }

    public void fillLessons() {

        if (((LinearLayout) lessonsList).getChildCount() > 0) {
            ((LinearLayout) lessonsList).removeAllViews();
        }

        ArrayList<DataObjects.Lesson> lessonList = ContentHelper.getTodaysLessons(lessons);
        if(lessonList == null) {
            // TODO show no cached data avaible (check if loading data card is not shown currently)
            return;
        }

        if(lessonList.size() == 0) {
            TextView message = (TextView) timetableError.findViewById(R.id.textView);
            message.setText(getString(R.string.no_lessons));
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)message.getLayoutParams();
            params.setMargins(0, 0, 0, 5);
            message.setLayoutParams(params);
            ViewUtility.changeViews(timetableView, timetableError, mShortAnimationDuration);

            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, CalendarFragment.newInstance(2))
                            .commit();
                }
            });
        }

        // Timetable
        for (int i = 0; i < lessonList.size(); i++) {
            View view = inflater.inflate(R.layout.timetable_main, null);

            DataObjects.Lesson entry = lessonList.get(i);

            TextView dateView = (TextView) view.findViewById(R.id.lesson_date);
            TextView dataView = (TextView) view.findViewById(R.id.lesson_data);
            TextView subDataView = (TextView) view.findViewById(R.id.lesson_sub_data);
            View colorView = (View) view.findViewById(R.id.lesson_color);

            dateView.setText(ViewUtility.getTimeStr(entry.startTime) + " - " + ViewUtility.getTimeStr(entry.endTime));
            dataView.setText(entry.title);
            subDataView.setText(entry.location);
            try {
                colorView.setBackgroundColor(Color.parseColor(entry.color));
            } catch (Exception e) {
                colorView.setBackgroundColor(Color.parseColor("#000000"));
            }

            lessonsList.addView(view);
        }
        //TODO if with internet connection
        ViewUtility.changeViews(timetableProgress, timetableView, mShortAnimationDuration);
    }

    public void fillTrains(){
        // Train
        if(trains.size() > 0) {
            DataObjects.Train entry = ContentHelper.getCurrentTrain(trains);
            if(entry == null) {
                // TODO show no cached data avaible (check if loading data card is not shown currently)
                return;
            }

            TextView lineView = (TextView) rootView.findViewById(R.id.train_line);
            TextView timeView = (TextView) rootView.findViewById(R.id.train_time_view);
            TextView destinationView = (TextView) rootView.findViewById(R.id.train_destination_view);

            lineView.setText(entry.line);
            lineView.setBackgroundColor(Color.parseColor(entry.color));
            destinationView.setText(entry.destination);

            Integer timeSpan = ViewUtility.getTimeSpan(entry.time);
            String time = ViewUtility.getTimeStr(entry.time);
            if((timeSpan < 11) & (timeSpan > 0)){
                time = String.format(getString(R.string.train_time_span), timeSpan);
            } else if (timeSpan == 0) {
                time = getString(R.string.train_time_now);
            }
            timeView.setText(time);
        }
        ViewUtility.changeViews(trainProgress, trainView, mShortAnimationDuration);
    }

    public void loadData(Boolean start) {
        if(!start) {
            ViewUtility.changeViews(cafeteriaView, cafeteriaProgress, mShortAnimationDuration);
            ViewUtility.changeViews(trainView, trainProgress, mShortAnimationDuration);
            ViewUtility.changeViews(timetableView, timetableProgress, mShortAnimationDuration);
        }

        new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/cafeteria/week/", "food"});
        new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/transportation/", "train"});
        new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/calendar/TINF13B2/week/", "lessons"});

        swipeLayout.setRefreshing(false);
    }

    private void showCache() {
        // train
        trains = ((ApplicationT) mActivity.getApplication()).getTrainData(); // returns null if not cached or outdated
        if(trains != null) {
            fillTrains();
        } else {
            trains = new ArrayList<DataObjects.Train>();
            ViewUtility.changeViews(trainView, trainProgress, mShortAnimationDuration);
        }

        // food
        meals = ((ApplicationT) mActivity.getApplication()).getFoodData(); // returns null if not cached or outdated
        if(meals != null) {
            fillMeals();
        } else {
            meals = new ArrayList<DataObjects.FoodDay>();
            ViewUtility.changeViews(cafeteriaView, cafeteriaProgress, mShortAnimationDuration);
        }

        // calendar
        lessons = ((ApplicationT) mActivity.getApplication()).getLessonData(); // returns null if not cached or outdated
        if(lessons != null) {
            fillLessons();
        } else {
            lessons = new ArrayList<DataObjects.Lesson>();
            ViewUtility.changeViews(timetableView, timetableProgress, mShortAnimationDuration);
        }
    }
}
