package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;


import java.util.ArrayList;

import android.app.Application;

public class ApplicationT extends Application {


    private ArrayList<DataObjects.Train> trainData;
    private ArrayList<DataObjects.FoodDay> foodData;
    private ArrayList<DataObjects.Lesson> lessonData;
    private ArrayList<DataObjects.DHBWNews> dhbwNews;
    private ArrayList<DataObjects.StuvNews> stuvNews;
    private ArrayList<DataObjects.Employee> employees;

    /**
     * Train Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.Train> getTrainData() {
        if(trainData == null){
            trainData = new ArrayList<DataObjects.Train>();
            trainData = CacheHelper.getObject("trainData", getFilesDir());
        }
        return trainData;
    }
    public void setTrainData(ArrayList<DataObjects.Train> var) {
        this.trainData = var;
        CacheHelper.saveObject(trainData, "trainData", getFilesDir());
    }

    /**
     * Food Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.FoodDay> getFoodData() {
        if(foodData == null){
            foodData = new ArrayList<DataObjects.FoodDay>();
            foodData = CacheHelper.getObject("foodData", getFilesDir());
        }
        return foodData;
    }
    public void setFoodData(ArrayList<DataObjects.FoodDay> var) {
        this.foodData = var;
        CacheHelper.saveObject(foodData, "foodData", getFilesDir());
    }

    /**
     * Lesson Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.Lesson> getLessonData() {
        if(lessonData == null){
            lessonData = new ArrayList<DataObjects.Lesson>();
            lessonData = CacheHelper.getObject("lessonData", getFilesDir());
        }
        return lessonData;
    }
    public void setLessonData(ArrayList<DataObjects.Lesson> var) {
        this.lessonData = var;
        CacheHelper.saveObject(lessonData, "lessonData", getFilesDir());
    }

    /**
     * DHBW News Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.DHBWNews> getDHBWNewsData() {
        if(dhbwNews == null){
            dhbwNews = new ArrayList<DataObjects.DHBWNews>();
            dhbwNews = CacheHelper.getObject("dhbwNewsData", getFilesDir());
        }
        return dhbwNews;
    }
    public void setDHBWNewsData(ArrayList<DataObjects.DHBWNews> var) {
        this.dhbwNews = var;
        CacheHelper.saveObject(dhbwNews, "dhbwNewsData", getFilesDir());
    }

    /**
     * Stuv News Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.StuvNews> getStuvNewsData() {
        if(stuvNews == null){
            stuvNews = new ArrayList<DataObjects.StuvNews>();
            stuvNews = CacheHelper.getObject("stuvNewsData", getFilesDir());
        }
        return stuvNews;
    }
    public void setStuvNewsData(ArrayList<DataObjects.StuvNews> var) {
        this.stuvNews = var;
        CacheHelper.saveObject(stuvNews, "stuvNewsData", getFilesDir());
    }

    /**
     * Emloyees Caching
     */
    @SuppressWarnings("unchecked")
    public ArrayList<DataObjects.Employee> getContactData() {
        if(employees == null){
            employees = new ArrayList<DataObjects.Employee>();
            employees = CacheHelper.getObject("employeesData", getFilesDir());
        }
        return employees;
    }
    public void setContactData(ArrayList<DataObjects.Employee> var) {
        this.employees = var;
        CacheHelper.saveObject(employees, "employeesData", getFilesDir());
    }

}