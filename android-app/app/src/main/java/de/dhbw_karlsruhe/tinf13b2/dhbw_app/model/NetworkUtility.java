package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class NetworkUtility {

    /**
     * check if device is online
     * @param ctx
     * @return
     */
    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    /**
     * open a url in android browser
     */
    public static void openBrowser(Context ctx, String URL) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
        ctx.startActivity(i);
    }

    /**
     * get JSON Data
     */
    public static String getJSONData(String _url){
        HttpGet httppost = new HttpGet(_url);

        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        // The default value is zero, that means the timeout is not used.
        int timeoutConnection = 3000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        int timeoutSocket = 5000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        InputStream inputStream = null;
        String result = null;
        try {
            HttpResponse response = httpClient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if(response.getStatusLine().getStatusCode() != 200) {
                return null;
            }

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            result = sb.toString();
        } catch (Exception e) {
            Log.e("DHBW", e.getMessage(), e);
            return "dataError";
        }
        finally {
            try{if(inputStream != null)inputStream.close();} catch(Exception squish){ Log.e("DHBW", squish.getMessage(), squish); }
            return result;
        }
    }
}