/*
 * Copyright (C) 2013 Onur-Hayri Bakici
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;


public abstract class InfinitePagerAdapter<T> extends PagerAdapter {

    public static final int PAGE_POSITION_LEFT = 0;
    public static final int PAGE_POSITION_CENTER = 1;
    public static final int PAGE_POSITION_RIGHT = 2;

    public static final int PAGE_COUNT = 3;

    public static final String SUPER_STATE = "super_state";
    public static final String ADAPTER_STATE = "adapter_state";

    private InfinitePageModel<T>[] mPageModels;

    private T mCurrentIndicator;

    /**
     * Standard constructor.
     *
     * @param initValue the initial indicator value the ViewPager should start with.
     */
    public InfinitePagerAdapter(T initValue) {
        mCurrentIndicator = initValue;

        mPageModels = new InfinitePageModel[PAGE_COUNT];
    }

    /**
     * This method is only called, when this pagerAdapter is initialized.
     */
    @Override
    public final Object instantiateItem(final ViewGroup container, final int position) {
        final InfinitePageModel<T> model = createPageModel(position);
        mPageModels[position] = model;
        container.addView(model.getParentView());
        return model;
    }

    /**
     * fills the page on index {@code position}.
     *
     * @param position the page index to fill the page.
     */
    void fillPage(final int position) {
        final InfinitePageModel<T> oldModel = mPageModels[position];
        final InfinitePageModel<T> newModel = createPageModel(position);
        if (oldModel == null || newModel == null) {
            return;
        }
        // moving the new created views to the page of the viewpager
        oldModel.removeAllChildren();
        for (final View newChild : newModel.getChildren()) {
            newModel.removeViewFromParent(newChild);
            oldModel.addChild(newChild);
        }

        mPageModels[position].setIndicator(newModel.getIndicator());
    }

    /**
     * Creates the internal page model. This method calls the {@link #instantiateItem(Object)} method
     * that creates the page content.
     *
     * @param pagePosition the position in the pageModel array between [0..2]
     * @return a new instance of a page model.
     */
    private InfinitePageModel<T> createPageModel(final int pagePosition) {
        final T indicator = getIndicatorFromPagePosition(pagePosition);
        final ViewGroup view = instantiateItem(indicator);

        return new InfinitePageModel<T>(view, indicator);
    }

    protected final T getCurrentIndicator() {
        return mCurrentIndicator;
    }

    private T getIndicatorFromPagePosition(final int pagePosition) {
        T indicator = null;
        switch (pagePosition) {
            case PAGE_POSITION_LEFT:
                indicator = getPreviousIndicator();
                break;
            case PAGE_POSITION_CENTER:
                indicator = getCurrentIndicator();
                break;
            case PAGE_POSITION_RIGHT:
                indicator = getNextIndicator();
                break;
        }
        return indicator;
    }

    /**
     * Package internal. Moves contents from page index {@code from} to page index {@code to}.
     *
     * @param from page index to move contents from.
     * @param to   page index to move contents to.
     */
    void movePageContents(final int from, final int to) {
        final InfinitePageModel<T> fromModel = mPageModels[from];
        final InfinitePageModel<T> toModel = mPageModels[to];
        if (fromModel == null || toModel == null) {
            return;
        }

        toModel.removeAllChildren();
        for (View view : fromModel.getChildren()) {
            fromModel.removeViewFromParent(view);
            toModel.addChild(view);
        }

        mPageModels[to].setIndicator(fromModel.getIndicator());
    }

    void reset() {
        for (InfinitePageModel<T> pageModel : mPageModels) {
            pageModel.removeAllChildren();
        }
    }

    /**
     * Sets {@code indicator} as the current visible indicator.
     *
     * @param indicator a indicator value.
     */
    void setCurrentIndicator(final T indicator) {
        mCurrentIndicator = indicator;
    }


    /**
     * @return the next indicator.
     */
    public abstract T getNextIndicator();


    /**
     * @return the previous indicator.
     */
    public abstract T getPreviousIndicator();

    /**
     * Instantiates a page.
     *
     * @param indicator the indicator the page should be instantiated with.
     * @return a ViewGroup containing the page layout.
     */
    public abstract ViewGroup instantiateItem(T indicator);

    /**
     * @param currentIndicator the current value of the indicator.
     * @return a string representation of the current indicator.
     * @see #convertToIndicator(String)
     */
    public String getStringRepresentation(final T currentIndicator) {
        return "";
    }

    /**
     * Convert the represented string back to its indicator
     *
     * @param representation the string representation of the current indicator.
     * @return the indicator.
     */
    public T convertToIndicator(final String representation) {
        return getCurrentIndicator();
    }

    @Override
    public final int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        final InfinitePageModel model = (InfinitePageModel) object;
        container.removeView(model.getParentView());
    }

    @Override
    public final boolean isViewFromObject(final View view, final Object o) {
        return view == ((InfinitePageModel) o).getParentView();
    }

}