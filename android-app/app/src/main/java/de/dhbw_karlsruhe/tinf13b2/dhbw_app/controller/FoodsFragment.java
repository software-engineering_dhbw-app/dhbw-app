package de.dhbw_karlsruhe.tinf13b2.dhbw_app.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FoodsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    protected Activity mActivity;

    private int section_number;

    private LayoutInflater inflater;
    private View rootView;


    private InfiniteViewPager mViewPager;

    public static FoodsFragment newInstance(int sectionNumber) {
        FoodsFragment fragment = new FoodsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.inflater = inflater;

        Bundle args = getArguments();
        this.section_number = args.getInt(ARG_SECTION_NUMBER);

        this.rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        mViewPager = (InfiniteViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(new MyInfinitePagerAdapter(0));
        mViewPager.setPageMargin(20);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        mActivity = activity;
    }

    private class MyInfinitePagerAdapter extends InfinitePagerAdapter<Integer> {

        private FoodDayListViewAdapter foodListViewAdapter;
        private ArrayList<DataObjects.FoodDay> weekFood;
        private ListView cardsListView;

        private View loadingCard;
        private ArrayList<DataObjects.FoodDay> allFood;
        private int mShortAnimationDuration;

        public MyInfinitePagerAdapter(final Integer initValue) {
            super(initValue);
        }

        @Override
        public ViewGroup instantiateItem(Integer indicator) {
            final LinearLayout layout = (LinearLayout) ((LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE))
                    .inflate(R
                            .layout
                            .fragment_calendar_adapter, null);

            allFood = ((ApplicationT) mActivity.getApplication()).getFoodData(); // returns null if not cached or outdated
            if(allFood == null) {
                new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/cafeteria/week/" + ContentHelper.getDayWeek(0), "0"});
            }

            this.loadingCard = (android.support.v7.widget.CardView) layout.findViewById(R.id.loading_card);
            mShortAnimationDuration = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);

            this.cardsListView = (ListView) layout.findViewById(R.id.cards_list);

            if (allFood == null) {
                allFood = new ArrayList<DataObjects.FoodDay>();
            }

            weekFood = ContentHelper.getWeekFood(allFood, indicator);
            if(weekFood != null) {
                foodListViewAdapter = new FoodDayListViewAdapter(mActivity.getApplicationContext(), weekFood);
                cardsListView.setAdapter(foodListViewAdapter);
                foodListViewAdapter.notifyDataSetChanged();
            } else {
                new JSONDataTask().execute(new String[]{"http://dhbw-api.it.dh-karlsruhe.de/v2/cafeteria/week/" + ContentHelper.getDayWeek(indicator), indicator.toString()});
            }

            layout.setTag(indicator);
            return layout;
        }

        @Override
        public Integer getNextIndicator() {
            return getCurrentIndicator() + 1;
        }

        @Override
        public Integer getPreviousIndicator() {
            return getCurrentIndicator() - 1;
        }

        @Override
        public String getStringRepresentation(final Integer currentIndicator) {
            return String.valueOf(currentIndicator);
        }

        @Override
        public Integer convertToIndicator(final String representation) {
            return Integer.valueOf(representation);
        }

        /* JSON Task */
        class JSONDataTask extends AsyncTask<String, Void, String> {

            private String type;
            private String url;

            protected String doInBackground(String... urls) {

                String jsonString = NetworkUtility.getJSONData(urls[0]);
                this.type = urls[1];
                this.url = urls[0];

                return jsonString;
            }

            protected void onPostExecute(String jsonString) {
                try {
                    if (jsonString == null || jsonString.equals("dataError")) {
                        ViewUtility.switchViews(loadingCard.findViewById(R.id.progress_view), R.layout.card_error, inflater);
                        ViewUtility.changeViews(cardsListView, loadingCard, mShortAnimationDuration);
                        return;
                    }

                    JSONArray jArray = new JSONArray(jsonString);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject oneObject = jArray.getJSONObject(i);

                        String date = oneObject.getString("date");
                        Boolean closed = oneObject.getBoolean("closed");
                        JSONArray mealsArray = oneObject.getJSONArray("meals");

                        ArrayList<DataObjects.Food> mealList = new ArrayList<DataObjects.Food>();
                        for (int j=0; j < mealsArray.length(); j++) {
                            JSONObject meal = mealsArray.getJSONObject(j);

                            String food_name = meal.getString("name");
                            String food_price = meal.getString("studentPrice");

                            DataObjects.Food entry = new DataObjects.Food(food_name, ViewUtility.formatPrice(food_price), "");
                            mealList.add(entry);
                        }

                        allFood.add(new DataObjects.FoodDay(date, mealList, closed));
                    }

                    ((ApplicationT) mActivity.getApplication()).setFoodData(weekFood);

                    weekFood = ContentHelper.getWeekFood(allFood, Integer.parseInt(this.type));
                    foodListViewAdapter = new FoodDayListViewAdapter(mActivity.getApplicationContext(), weekFood);
                    if(weekFood != null){
                        cardsListView.setAdapter(foodListViewAdapter);
                        foodListViewAdapter.notifyDataSetChanged();
                        ViewUtility.changeViews(loadingCard, cardsListView, mShortAnimationDuration);
                    } else {
                        ViewUtility.switchViews(loadingCard.findViewById(R.id.progress_view), R.layout.card_no_information, inflater);
                        ViewUtility.changeViews(cardsListView, loadingCard, mShortAnimationDuration);
                    }
                } catch (JSONException e) {
                    Log.e("DHWB", "JSON: " + jsonString);
                    Log.e("DHBW", e.getMessage(), e);
                }
            }
        }
    }
}