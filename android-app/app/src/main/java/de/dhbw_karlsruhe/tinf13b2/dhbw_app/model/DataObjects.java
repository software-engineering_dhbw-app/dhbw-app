package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DataObjects implements Serializable {

    public static class FoodDay implements Serializable {
        public String date;
        public ArrayList<Food> meals;
        public Boolean closed;

        public FoodDay(String date, ArrayList<Food> meals, Boolean closed) {
            this.date = date;
            this.meals = meals;
            this.closed = closed;
        }
    }

    public static class Food implements Serializable {
        public String name;
        public String price;
        public String additional;

        public Food(String name, String price, String additional) {
            this.name = name;
            this.price = price;
            this.additional = additional;
        }
    }

    public static class Train implements Serializable {
        public String line;
        public String destination;
        public String time;
        public String color;


        public Train(String line, String destination, String time, String color) {

            this.line = line;
            this.destination = destination;
            this.time = time;
            this.color = color;
        }
    }

    public static class LessonDay implements Serializable {
        public String date;
        public ArrayList<Lesson> lessons;

        public LessonDay(String date, ArrayList<Lesson> lessons) {
            this.date = date;
            this.lessons = lessons;
        }
    }

    public static class Lesson implements Serializable {
        public String category;
        public String title;
        public String person;
        public String location;
        public String startTime;
        public String endTime;
        public String color;


        public Lesson(String category, String title, String person, String location, String startTime, String endTime, String color) {

            this.category = category;
            this.title = title;
            this.person = person;
            this.location = location;
            this.startTime = startTime;
            this.endTime = endTime;
            this.color = color;
        }
    }

    public static class DHBWNews implements Serializable {
        public String content;
        public String published;
        public String link;
        public String description;
        public String title;

        public DHBWNews(String content, String published, String link, String description, String title) {

            this.content = content;
            this.published = published;
            this.link = link;
            this.description = description;
            this.title = title;
        }
    }

    public static class StuvNews implements Serializable {
        public String content;
        public String published;
        public String link;
        public String title;

        public StuvNews(String content, String published, String link, String title) {

            this.content = content;
            this.published = published;
            this.link = link;
            this.title = title;
        }
    }

    public static class Employee implements Serializable {
        public String name;
        public String function;
        public String telephone;
        public String room;

        public Employee(String name, String function, String telephone, String room) {

            this.name = name;
            this.function = function;
            this.telephone = telephone;
            this.room = room;
        }
    }
}
