package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.dhbw_karlsruhe.tinf13b2.dhbw_app.R;

import java.util.ArrayList;

public class LessonsDayListViewAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<DataObjects.LessonDay> lessons;

    public LessonsDayListViewAdapter(Context mContext, ArrayList<DataObjects.LessonDay> lessons) {
        this.mContext = mContext;
        this.lessons = lessons;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.list_calendar_main, null);

        final DataObjects.LessonDay entry = lessons.get(position);

        TextView dateView = (TextView) view.findViewById(R.id.date);
        LinearLayout listView = (LinearLayout) view.findViewById(R.id.cards_list);

        dateView.setText(entry.date);

        for (DataObjects.Lesson singleLesson : entry.lessons) {
            View vi = LayoutInflater.from(mContext).inflate(R.layout.timetable_main, null);

            TextView timeView = (TextView) vi.findViewById(R.id.lesson_date);
            TextView dataView = (TextView) vi.findViewById(R.id.lesson_data);
            TextView subDataView = (TextView) vi.findViewById(R.id.lesson_sub_data);
            View colorView = (View) vi.findViewById(R.id.lesson_color);

            timeView.setText(ViewUtility.getTimeStr(singleLesson.startTime) + " - " + ViewUtility.getTimeStr(singleLesson.endTime));
            dataView.setText(singleLesson.title);
            subDataView.setText(singleLesson.location);
            try {
                colorView.setBackgroundColor(Color.parseColor(singleLesson.color));
            } catch (Exception e) {
                colorView.setBackgroundColor(Color.parseColor("#000000"));
            }

            listView.addView(vi);
        }

        return view;
    }

    @Override
    public int getCount() {
        return lessons.size();
    }

    @Override
    public Object getItem(int position) {
        return lessons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
