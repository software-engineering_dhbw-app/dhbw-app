package de.dhbw_karlsruhe.tinf13b2.dhbw_app.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebViewClient extends WebViewClient {

    private Context ctx;

    public CustomWebViewClient(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if(url.contains("fake://fake.de")) {
            view.loadUrl(url);
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            this.ctx.startActivity(i);
        }
        return true;
    }

    public void setContext (Context ctx) {
        this.ctx = ctx;
    }
}