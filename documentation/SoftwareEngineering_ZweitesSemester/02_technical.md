## Gliederung

1. Business Case
2. <span class="fragment highlight-blue">Technische Übersicht</span>
3. Projektplanung
4. Qualität

Note: Sebastian

--------------------

## Demo

<a href="http://localhost:8000/demo.html" data-preview-link>appetize</a>

[mobizen](https://www.mobizen.com/)

Note: Sebastian

--------------------

# Klassendiagramm

--------------------

<!-- .slide: data-background="images/class-diagramm.png" data-background-size="100%"-->

Note: Julien

MVC
- Klare Trennung nach Aufgaben
- verständliche Architektur
- leichter zu testen, bearbeiten

verschiedene Implementierungen