## Gliederung

1. Business Case
2. Technische Übersicht
3. Projektplanung
4. <span class="fragment highlight-blue">Qualität</span>

Note: Julien

--------------------

## Architektur

Note: Julien, überspringen

Why an Architecture?
* Documentation of code
* Easier to navigate to relevant code
* Easier to Test
* Automatic generation of diagrams (objectAid)
* Living document

--------------------

<!-- .slide: data-background="images/network.png" data-background-size="80%"-->

Note: Sebastian

- Server stellt zentrale Stelle für die App dar
- bündelt alle Informationen der verschiedenen Service Provider
- so stellen wir, bei Änderungen der Service Provider sicher, dass die App auch ohne ein Update weiterhin funktionieren kann indem wir lediglich an einer zentralen Stelle Änderungen durchführen müssen
- zudem wird die Last auf den Geräten und den Service Providern minimiert, da der Server alle Anfragen cached

--------------------

## Konfiguration

| **Bitbucket** |     **Jira**    |
|---------------|-----------------|
| Programmcode  | Use Cases       |
| Präsentation  | Change Requests |
| Diagramme     |                 |
| Installation  |                 |

Note: Sebastian

Konfigurationsmanagament im Allgemeinen übernimmt die Nachverfolgung aller Änderungen im Projekt und somit werden diese dann sichtbar gemacht

- auf Bitbucket hosten wir sämtliche Dateien im Git Repository, darunter der eigentliche Programmcode aber auch diese Präsentation, Diagramme oder auch Informationen zur Installation
- in Jira hingegen verwalten wir alle Change Requests und Aufgaben, die Use Cases betreffend

--------------------

## Konfiguration

```
api:
  build: .
  links:
   - db
  environment:
   - MODE=DEVELOPMENT
  ports:
   - "5000:5000"
  volumes:
   - .:/usr/src/app:ro
   - /tmp/flask-logs:/logs
   - /etc/localtime:/etc/localtime:ro
db:
  image: redis
```
Docker

Note: Julien

--------------------

## Automation

--------------------

<!-- .slide: data-background="images/bamboo.png" data-background-size="80%"-->

Note: Sebastian
- Bamboo unterstützt uns in der Continous Integration
- sobald Änderungen zu Bitbucket comitted werden, führt Bamboo einen automatischen Build aus
- somit können Fehler direkt erkannt werden und Tests müssen nicht extra ausgeführt werden bzw. können nicht vergessen werden

--------------------

![](images/readthedocs.png)

Read The Docs

Note: Julien

--------------------

## Testing

![](images/bamboo-tests.png)

Note: Julien

Unit Tests:
- einzelne Teile des Codes

Functional Tests:
- Teile des Codes interagieren
- calabash-android, behave

--------------------

![](images/runscope-performance.png)
Runscope

Note: Julien

--------------------

## Integration

![](images/jira-ide-2.png)

![](images/bamboo-ide-2.png)

Note: Sebastian

- durch die von Atlassian verfügbaren Plugins für IntelliJ konnten wir Bamboo und Jira in die IDE integrieren und direkt auf die Daten zugreifen
- außerdem konnten wir so zu jeglichen Jira Tasks die integrierte Time Tracking Funktion nutzen und genau zuordnen wie viel Zeit wir für welchen Use Case benötigt haben

--------------------

## Risk Managment

--------------------

<!-- .slide: data-background="images/risk-management.png" data-background-size="70%"-->

Note: Sebastian

- Für das Risk Managament haben wir vier Phasen
- Identifizieren, Analysieren, Bewerten wie wichtig die Risiken sind und sie anschließend behandeln
- hierdurch können wir Risiken schnell lösen

--------------------

## Risk Managment

* Der App-User hat meist nur eine schlechte Internetverbindung
* Die Informationsquellen ändern sich
* Ein Team-Mitglied wird krank

Note: Sebastian

--------------------

## Design Pattern

Note: Sebastian

- Stellen Lösungen zu bekannten Software Problemen dar
- so können wir auf bewehrte Lösungen zu Problemen zurückgreifen und arbeiten effizienter
- außerdem wird das System hierdurch flexibler 

--------------------

## Design Pattern

```html
<table cellspacing="0" cellpadding="0" class="easy-tab-dot" >
<tr class="handicap">
<td style="background: white;width:115px;" valign="top">[VEG]</td>
<td class="first dot" valign="top" ><span class="bg">
<b>Gemüseschnitzel</b>

<span>Käse-Sahnesoße</span> 
(1,Gl,ML)</span>
</td><td style="text-align: right;vertical-align:bottom;;" >
<span class="bg">2,00 &euro;</span>
```

Note: Julien

--------------------

## Design Pattern

![](images/facade-pattern.png)

Facade

Note: Julien

- Einfacher Zugriff auf eine Menge von Code
- besser verständlich

--------------------

## Design Pattern

```
{
  "category": "Wahlessen 2", 
  "studentPrice": 2.0, 
  "name": "Gemüseschnitzel Käse-Sahnesoße", 
  "condiments": ["1", "Gl", "ML"]
}
```

Note: Julien

--------------------

## Metrics

![](images/lint-report-2.png)

![](images/pylint-module-2.png)

Note: Julien

Lint, pylint

Goal of Metrics:
Product Revision
* Maintainability
* Flexibility
* Testability
Product Transition
* Portability
* Reusability
* Interoperability
Product Operation
* Correctness
* Reliability
* Usability
* Integrity
* Efficiency