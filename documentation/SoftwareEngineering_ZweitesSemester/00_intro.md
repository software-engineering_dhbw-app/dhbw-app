# DHBW-APP

A modern Android App for DHBW-Karlsruhe

Note: Sebastian

--------------------

## Team

| **Sebastian Dernbach** |  **Julien Hadley Jack** |
|------------------------|-------------------------|
| Lead App-Architekt     | Lead Backend-Architekt  |
| Lead App-Entwickler    | Lead Backend-Entwickler |
| UI-Designer für App    | Projektmanager          |
| Tester                 | Tester                  |

Note: Julien, Sebastian
