## Project Vision

* Schneller Überblick
* Einheitliche Oberfläche
* Offline verfügbar

Note: Material Design

--------------------

# IST-Zustand

* Keine nativen Elemente
* Layoutfehler
* Abstürze

--------------------

# Das Team

* Julien Hadley Jack (Lead API-Architekt, Lead API-Entwickler, Projektmanager)
* Sebastian Dernbach (Lead App-Architekt, Lead App-Entwickler, UI-Designer für App)
