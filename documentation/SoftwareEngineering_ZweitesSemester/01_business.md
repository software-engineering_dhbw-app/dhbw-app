## Gliederung

1. <span class="fragment highlight-blue">Business Case</span>
2. Technische Übersicht
3. Projektplanung
4. Qualität

Note: Julien

--------------------

## Projekt Vision

Android-App, die Studenten beim täglichen Besuch der DHBW Karlsruhe unterstützt

Note: Sebastian
--------------------

## Projekt Vision

|                  |                  |
|------------------|------------------|
| S-Bahn           | Stundenplan      |
| Mensa            | Mitarbeiter      |
| DHBW Nachrichten | StuV Nachrichten |
| Raumsuche        |                  |
|                  |                  |

Note: Julien

--------------------

## Use Cases

--------------------

<!-- .slide: data-background="images/use-case-overview.png" data-background-size="50%"-->

Note: Sebastian

- Use Case Diagramm
- Akteure (dunkel: Service Provider)
- Scope der Use Cases
- Recherche weiterer Funktionen