## Gliederung

1. Business Case
2. Technische Übersicht
3. <span class="fragment highlight-blue">Projektplanung</span>
4. Qualität

Note: Sebastian

--------------------

## Short term

![Sprint](images/sprint.png)

Note: Sebastian

SCRUM
Fokus auf effizientes Arbeiten
Weg entwickelt sich während der Implementierung
Schnell reagieren
regelmäßige Veröffentlichung
Leicht Entscheidungen treffen

Sprint
designed, programmiert und getestet
Product Backlog
Sprint Backlog

--------------------

<!-- .slide: data-background="images/jira.png" data-background-size="80%"-->

Note: Sebastian

Vorrausplanung und Koordinierung von Aufgaben 
Aufgaben direkt zuordnen
schnelleres Arbeiten

--------------------

## Long term

--------------------

<!-- .slide: data-background="images/rup.gif" data-background-size="80%"-->

Note: Julien

Rational Unified Process (RUP)

4 Phasen unterteilt in mehrere Iterationen
Arbeitsschritte werden in 9 Disziplinen unterteilt

GanttProject

--------------------

<!-- .slide: data-background="images/projectplan.png" data-background-size="100%"-->

Note: Julien

Inception, Elaboration, Construction, Transition

--------------------

## Cost Estimation

![Function Points](images/function-points.png)

Note: Sebastian

TODO: Erklärung Function Points

- Function Points dienen zur Abschätzung von zukünftigem Aufwand 
- wir haben für uns das Problem gesehen, dass unsere Use Cases oft den selben In- und Output besitzen, aber trotzdem stark im Aufwand variieren. 
- Wie man in der Demo eben auf dem Dashboard gesehen hat, sind die Karten mit den gleichen Function Points verbunden, jedoch kann der Aufwand um die Daten von externen Dienstleistern zu beziehen doppelt so hoch sein. 
- Deshalb wollten wir uns mehr auf das Wissen der Entwickler stützen und haben deshalb das Planning Poker Verfahren ausgewählt

FP are measured from a functional, or user, point of view. 


--------------------

## Planning Poker

![Planning Poker](images/planning-poker-cards.jpg)

Note: Sebastian

- Im Planning Poker Verfahren werden Use-Cases durch die Programmierer abgeschätzt und durch Punkte bewertet, dabei wird lediglich nach Arbeitsaufwand abgeschätzt
- für die Punkte werden verschiedene Zahlen festgelegt, wie man hier auf dem Bild sehen kann, die Zahl ansich ist egal, es müssen nur für Use Cases mit ungefähr gleichem Aufwand auch die gleiche Zeit genutzt werden

--------------------

## Planning Poker

![Planning Poker](images/planning-poker.png)

Note: Sebastian

- Hier sieht man das entstandene Diagram durch die Abschätzung durch Planning Poker. Man sieht, dass fast alle Use Cases auf der Trendlinie liegen, lediglich List und Search Contact Persons liegen etwas außerhalb
- Search Contact Persons war aufgrund der Filter Funktionalität, die Android mit sich bringt einfacher als wir erwartet haben, bei List Contact Person kam nach der Abschätzung noch die Swipe Funktionalität hinzu, was den Aufwand erhöht hat.

--------------------

# Change Request

Note: Julien

Change Requests werden benötigt um eine einfache Kommunikation mit dem Kunden zu ermögliche, da sich Anforderungen mit der Zeit ändern können

Änderungen: Zeit, Geld, potenzielle neue Bugs

--------------------

<!-- .slide: data-background="images/change-request.png" data-background-size="50%"-->

Note: Julien

- Kunde öffnet Ticket
- Zentral in Jira bearbeitet
- Kunde bestätigt Umsetzung nach Vorstellung

--------------------

<!-- .slide: data-background="images/change-request-workflow.svg" data-background-size="55%"-->

Note: Julien

Rollen: Customer, Project Lead, Component Lead