.. DHBW-API documentation master file, created by
   sphinx-quickstart on Sat Dec  6 12:59:11 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

####################################
Welcome to DHBW-API's documentation!
####################################

.. toctree::
   :maxdepth: 2

   content/server
   content/error
   content/resources
   content/api