======
Server
======

This API can be installed by following the install instrutions on `the repository <https://bitbucket.org/software-engineering_dhbw-app/dhbw-app/src/master/server-backend/>`_.

You can also use this installation:

 * http://dhbw-api.it.dh-karlsruhe.de/v2/

