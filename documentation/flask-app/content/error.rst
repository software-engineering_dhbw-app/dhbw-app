==============
Error handling
==============

.. note::

   Error handling is still a work in progress and most error won't be caught at the moment.

Normally the server will respond with an status code :http:statuscode:`200`. When an error code happens, one the the following status codes will be used:

* :http:statuscode:`400`: At least one of the paramters given to API wasn't correct. This could be because it is missing or in the wrong format.
* :http:statuscode:`500`: There was an error trying to get the wanted information.

Also an JSON object will be returned with more information about the error:

   .. sourcecode:: http

      HTTP/1.1 400 Bad Request
      Content-Type: application/json;charset=utf-8

      {
       "errorCode": 100, 
       "message": "The information provided for course is in the wrong format (given: 2015-04-15).", 
       "reference": "http://dhbw-app.readthedocs.org/en/latest/#document-content/error", 
       "statusCode": 400
      }

The error contains:

* **errorCode** (int): A unique id for the category of error.
* **message** (str): A message which should be used for logging by the client. The message isn't meant to be shown to the user directly.
* **reference** (str): A link to the documentation with general information about error handling by the API.
* **statusCode** (int): The http status code that was returned with this error.

