=============
API Endpoints
=============

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :endpoints: help_text

Cafeteria
=========

.. note::

   Doesn't show days in the past.



.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: cafeteria

Calendar
========

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: calendar

Contact
=======

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: contact

DHBW News
=========

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: dhbw_news

StuV News
=========

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: stuv_news

Transportation
==============

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: transportation

Rooms
=====

.. autoflask:: flask_app:create_app()
   :include-empty-docstring:
   :blueprints: rooms
