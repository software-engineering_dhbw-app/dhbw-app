================
Common resources
================

Date, Time
==========

.. note::

   Currently the time zone information isn't consistently used throughout the API. This will be corrected in a future version.

Dates and timestamps are expected and returned in the `ISO 8601 format <http://www.iso.org/iso/home/standards/iso8601.htm>`_. Examples are:

* *2015-04-14* (Date)
* *2015-04-14T19:24:05* (Combined date and time with time zone information)
* *2015-04-14T19:24:05+20:00* (Combined date and time with time zone information)


.. todo::

    Add timezone information to all time resources
