# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
from io import open
import json

from fuzzywuzzy import fuzz
from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock

from flask.ext.app.model.factory import StuvNewsFactory
from tests import FlaskAppTestCase, implementation_name, for_implementations, TEST_PATH


class StuvNewsTestCase(FlaskAppTestCase):
    factory = StuvNewsFactory

    def setUp(self):
        super(StuvNewsTestCase, self).setUp()

        self.maxDiff = None

    def _check_article(self, article):
        self.assertIsInstance(article, dict)
        dict_keys = ['content', 'link', 'published', 'title']
        self.assertListEqual(sorted(article.keys()), dict_keys)

        for key in dict_keys:
            self.assert_(article.get(key), article)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_news(self, implementation):
        if not hasattr(implementation, "get_news"):
            pass

        with open(TEST_PATH + "/data/stuv_news/news_expected.txt") as f:
            expected = json.load(f)

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/stuv_news/news.txt") as f:
                m.get("https://www.facebook.com/feeds/page.php?format=rss20&id=166591256706746", text=f.read())

            articles = implementation.get_news()

        for article1, article2 in zip(articles, expected):
            for key in ['link', 'published', 'title']:
                self.assertEqual(article1[key], article2[key])

            # the facebook urls have a unique identifier in the parameters which changes every time
            self.assertGreater(fuzz.ratio(article1['content'], article2['content']), 90)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_news_live(self, implementation):
        if not hasattr(implementation, "get_news"):
            pass

        articles = implementation.get_news()

        self.assertGreater(len(articles), 0)
        for article in articles:
            self._check_article(article)


if __name__ == '__main__':
    unittest.main()