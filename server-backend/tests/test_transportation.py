# -*- coding: utf-8 -*-
import unittest
from io import open
import json
import datetime

from dateutil import tz
from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock
import mock

from flask.ext.app.custom_exceptions.api_exeption import InvalidAPIUsage
from flask.ext.app.model.factory import TransportationFactory
from flask.ext.app.model.transportation import KvvApiTransportation, AbstractTransportation
from tests import FlaskAppTestCase, implementation_name, for_implementations, TEST_PATH


class TransportationTestCase(FlaskAppTestCase):
    factory = TransportationFactory

    def setUp(self):
        super(TransportationTestCase, self).setUp()

        self.maxDiff = None

    @staticmethod
    def _mock_now():
        return datetime.datetime(2015, 5, 8, 21, 0, tzinfo=tz.tzlocal())

    def _check_connection(self, connection):
        self.assertIsInstance(connection, dict)
        dict_keys = ['destination', 'line', 'lineColor', 'time']
        self.assertListEqual(sorted(connection.keys()), dict_keys)

        for key in dict_keys:
            self.assert_(connection.get(key), connection)

        color = int(connection['lineColor'][1:], 16)
        self.assertLessEqual(color, 0xFFFFFF)

    @parameterized.expand(for_implementations(AbstractTransportation), testcase_func_name=implementation_name)
    def test_convert_time(self, implementation):
        difference = implementation._now() - datetime.datetime.now(tz.tzlocal())
        self.assert_(difference < datetime.timedelta(minutes=1), difference)

    @parameterized.expand(for_implementations(KvvApiTransportation), testcase_func_name=implementation_name)
    @mock.patch.object(AbstractTransportation, '_now', _mock_now)
    def test_convert_time(self, implementation):
        now = self._mock_now().replace(second=0, microsecond=0)
        test_data = {
            "23:54": now.replace(hour=23, minute=54).isoformat(),
            "00:00": (now.replace(hour=0, minute=0) + datetime.timedelta(days=1)).isoformat(),
            "00:03": (now.replace(hour=0, minute=3) + datetime.timedelta(days=1)).isoformat(),
            "4 min": (now + datetime.timedelta(minutes=4)).isoformat()
        }

        for data, expected in test_data.items():
            self.assertEqual(implementation._convert_time(data), expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @mock.patch.object(AbstractTransportation, '_now', _mock_now)
    def test_get_next(self, implementation):
        if not hasattr(implementation, "get_next"):
            pass

        with open(TEST_PATH + "/data/transportation/next_expected.txt") as f:
            expected = json.load(f)

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/transportation/next.txt") as f:
                m.get("http://live.kvv.de/webapp/departures/bystop/de:8212:12?key=377d840e54b59adbe53608ba1aad70e8",
                      text=f.read())

            connections = implementation.get_next()

        self.assertEqual(connections, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_next_live(self, implementation):
        if not hasattr(implementation, "get_next"):
            pass

        connections = implementation.get_next()

        self.assertGreater(len(connections), 0)
        for connection in connections:
            self._check_connection(connection)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_next_error(self, implementation):
        if not hasattr(implementation, "get_next"):
            return

        with requests_mock.mock() as m:
            m.get("http://live.kvv.de/webapp/departures/bystop/de:8212:12?key=377d840e54b59adbe53608ba1aad70e8",
                  status_code=404)

            with self.assertRaises(InvalidAPIUsage) as cm:
                implementation.get_next()
            self.assertEqual(cm.exception.status_code, 404)


if __name__ == '__main__':
    unittest.main()