# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
import datetime
from io import open
import json

from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock

from flask.ext.app.custom_exceptions.api_exeption import InvalidAPIUsage
from flask_app.lib.pyiCalendar import pyiCalendar
from flask.ext.app.model.calendar_ import IcalCalendar
from flask.ext.app.model.factory import CalendarFactory
from tests import FlaskAppTestCase, implementation_name, for_implementations, TEST_PATH


class CalendarTestCase(FlaskAppTestCase):
    factory = CalendarFactory

    def setUp(self):
        super(CalendarTestCase, self).setUp()
        self.now = datetime.datetime.now()
        self.now_format = self.now.strftime("%Y-%m-%d")

        self.maxDiff = None

    def _check_event(self, event):
        self.assertIsInstance(event, dict)
        dict_keys = ['category', 'course', 'endTime', 'location', 'person', 'startTime', 'title']
        self.assertListEqual(sorted(event.keys()), dict_keys)

        for key in dict_keys:
            self.assert_(event.get(key), "{}\n{}".format(key, event))

    @parameterized.expand(for_implementations(IcalCalendar), testcase_func_name=implementation_name)
    def test_event_map(self, implementation):
        cal = pyiCalendar.iCalendar()
        cal.local_load(TEST_PATH + "/data/calendar/TINF13B2.ics")

        result = cal.get_event_instances(start="20150501", end="20150510")
        result = map(implementation._event_map, result)
        with open(TEST_PATH + "/data/calendar/TINF13B2_expected.txt") as f:
            expected = json.load(f)

        self.assertListEqual(result, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_day(self, implementation):
        if not hasattr(implementation, "get_day"):
            return

        with open(TEST_PATH + "/data/calendar/2015_05_04_day_expected.txt") as f:
            expected = json.load(f)
        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/calendar/TINF13B2.ics") as f:
                m.get("https://rapla.dhbw-karlsruhe.de/rapla?page=iCal&user=freudenmann&file=TINF13B2", text=f.read())
            with open(TEST_PATH + "/data/calendar/2015_05_04.txt") as f:
                m.get(
                    "https://rapla.dhbw-karlsruhe.de/rapla?page=calendar&user=freudenmann&file=TINF13B2&day=4&month=5&year=2015",
                    text=f.read())

            day = implementation.get_day("TINF13B2", "2015-05-04")

        # self.assertEqual(day, expected, "\n" + pprint.pformat(day))
        self.assertEqual(day, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_week(self, implementation):
        if not hasattr(implementation, "get_week"):
            return

        with open(TEST_PATH + "/data/calendar/2015_05_04_week_expected.txt") as f:
            expected = json.load(f)
        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/calendar/TINF13B2.ics") as f:
                m.get("https://rapla.dhbw-karlsruhe.de/rapla?page=iCal&user=freudenmann&file=TINF13B2", text=f.read())
            with open(TEST_PATH + "/data/calendar/2015_05_04.txt") as f:
                m.get(
                    "https://rapla.dhbw-karlsruhe.de/rapla?page=calendar&user=freudenmann&file=TINF13B2&day=4&month=5&year=2015",
                    text=f.read())

            week = implementation.get_week("TINF13B2", "2015-05-04")

        self.assertListEqual(week, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_day_live(self, implementation):
        if not hasattr(implementation, "get_day"):
            pass

        events = implementation.get_day("TINF13B2", self.now_format)

        for event in events:
            self._check_event(event)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_week_live(self, implementation):
        if not hasattr(implementation, "get_week"):
            pass

        events = implementation.get_week("TINF13B2", self.now_format)

        for event in events:
            self._check_event(event)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_day_error(self, implementation):
        if not hasattr(implementation, "get_day"):
            return

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("TINF13B2", "")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("TINF13B2", "2015-04")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("TINF13B2", "2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("", "2015-04-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("TINF113B2", "2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("123456", "2015-04-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day("", "")
        self.assertEqual(cm.exception.status_code, 400)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_week_plan_error(self, implementation):
        if not hasattr(implementation, "get_week"):
            return

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("TINF13B2", "")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("TINF13B2", "2015-04")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("TINF13B2", "2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("", "2015-04-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("TINF113B2", "2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("123456", "2015-04-02")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week("", "")
        self.assertEqual(cm.exception.status_code, 400)

if __name__ == '__main__':
    unittest.main()