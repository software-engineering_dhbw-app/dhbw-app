# -*- coding: utf-8 -*-
import unittest
from io import open
import json

from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock

from flask.ext.app.model.factory import DhbwNewsFactory
from tests import FlaskAppTestCase, implementation_name, for_implementations, TEST_PATH


class DhbwNewsTestCase(FlaskAppTestCase):
    factory = DhbwNewsFactory

    def setUp(self):
        super(DhbwNewsTestCase, self).setUp()

        self.maxDiff = None

    def _check_article(self, article):
        self.assertIsInstance(article, dict)
        dict_keys = ['content', 'description', 'link', 'published', 'title']
        self.assertListEqual(sorted(article.keys()), dict_keys)

        for key in dict_keys:
            self.assert_(article.get(key), article)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_news(self, implementation):
        if not hasattr(implementation, "get_news"):
            pass

        with open(TEST_PATH + "/data/dhbw_news/news_expected.txt") as f:
            expected = json.load(f)

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/dhbw_news/news.txt") as f:
                m.get("http://www.dhbw-karlsruhe.de/index.php?id=313&type=100", text=f.read())

            articles = implementation.get_news()

        self.assertEqual(articles, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_news_live(self, implementation):
        if not hasattr(implementation, "get_news"):
            pass

        articles = implementation.get_news()

        self.assertGreater(len(articles), 0)
        for article in articles:
            self._check_article(article)


if __name__ == '__main__':
    unittest.main()