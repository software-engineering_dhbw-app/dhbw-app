# -*- coding: utf-8 -*-
import datetime
import json
import unittest
from io import open
import pprint

from bs4 import BeautifulSoup
import mock
from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock

from flask.ext.app.custom_exceptions.api_exeption import InvalidAPIUsage
from flask.ext.app.model.cafeteria import AbstractCafeteria, JsonCafeteria, HtmlCafeteria
from flask.ext.app.model.factory import CafeteriaFactory
from tests import FlaskAppTestCase, for_implementations, implementation_name, TEST_PATH


class CafeteriaTestCase(FlaskAppTestCase):
    factory = CafeteriaFactory

    def setUp(self):
        super(CafeteriaTestCase, self).setUp()
        self.now = datetime.datetime.now()
        self.now_format = self.now.strftime("%Y-%m-%d")

        self.maxDiff = None

    @staticmethod
    def _mock_now():
        return datetime.datetime(2015, 1, 8, 21, 0)

    @parameterized.expand(for_implementations(AbstractCafeteria), testcase_func_name=implementation_name)
    def test_format_date(self, implementation):
        test_data = [
            [{"date": datetime.datetime(2000, 12, 1)}, "2000-12-01"],
            [{"date": datetime.datetime(2010, 03, 15, 20, 35)}, "2010-03-15"],
            [{"date": datetime.datetime(1990, 1, 1), "day_offset": 1}, "1990-01-02"],
            [{"date": datetime.datetime(1999, 5, 20), "day_offset": 0}, "1999-05-20"],
            [{"date": datetime.datetime(1995, 2, 4), "day_offset": -1}, "1995-02-03"],
            [{"date": datetime.datetime(2002, 1, 31), "day_offset": 3}, "2002-02-03"],
            [{"date": datetime.datetime(2004, 6, 12), "format_": "%Y"}, "2004"],
            [{"date": datetime.datetime(1980, 9, 11), "format_": "%m"}, "09"],
            [{"date": datetime.datetime(2004, 6, 12), "format_": None}, datetime.datetime(2004, 6, 12)],
            [{"date": datetime.datetime(1997, 03, 31), "day_offset": 3, "format_": "%Y"}, "1997"],
            [{"date": datetime.datetime(1985, 11, 3), "day_offset": 3, "format_": None}, datetime.datetime(1985, 11, 6)]
        ]

        for args, expected in test_data:
            self.assertEqual(implementation._format_date(**args), expected)

    @parameterized.expand(for_implementations(AbstractCafeteria), testcase_func_name=implementation_name)
    def test_get_weekdays(self, implementation):
        test_data = [
            [{"date": datetime.datetime(2020, 12, 1)},
             ['2020-11-30', '2020-12-01', '2020-12-02', '2020-12-03', '2020-12-04', '2020-12-05', '2020-12-06']],
            [{"date": datetime.datetime(2023, 03, 15, 20, 35)},
             ['2023-03-13', '2023-03-14', '2023-03-15', '2023-03-16', '2023-03-17', '2023-03-18', '2023-03-19']],
            [{"date": datetime.datetime(2035, 6, 12), "format_": "%Y"},
             ['2035', '2035', '2035', '2035', '2035', '2035', '2035']],
            [{"date": datetime.datetime(2028, 9, 11), "format_": "%m"},
             ['09', '09', '09', '09', '09', '09', '09']],
            [{"date": datetime.datetime(2046, 6, 12), "format_": None},
             [datetime.datetime(2046, 6, 11, 0, 0), datetime.datetime(2046, 6, 12, 0, 0),
              datetime.datetime(2046, 6, 13, 0, 0), datetime.datetime(2046, 6, 14, 0, 0),
              datetime.datetime(2046, 6, 15, 0, 0), datetime.datetime(2046, 6, 16, 0, 0),
              datetime.datetime(2046, 6, 17, 0, 0)]]
        ]

        for args, expected in test_data:
            result = implementation._get_weekdays(**args)
            self.assertEqual(len(result), 7, result)
            self.assertEqual(result, expected)

    @parameterized.expand(for_implementations(AbstractCafeteria), testcase_func_name=implementation_name)
    def test_get_weekdays2(self, implementation):
        now = datetime.datetime.now()
        result = implementation._get_weekdays(now)
        self.assertEqual(len(result), 7 - now.weekday(), result)

        past_day = now - datetime.timedelta(days=7)
        print(past_day)
        result = implementation._get_weekdays(past_day)
        self.assertEqual(len(result), 0, result)

    def _check_day(self, day):
        self.assertIsInstance(day, dict)
        self.assertListEqual(sorted(day.keys()), ["closed", "date", "meals"])

        if day["closed"]:
            self.assertEqual(len(day["meals"]), 0, day)
        else:
            self.assertGreater(len(day["meals"]), 0, day)

        for meal in day["meals"]:
            self.assertListEqual(meal.keys(), ["category", "studentPrice", "name", "condiments"])

            self.assert_(meal["category"])
            self.assert_(meal["name"])

            self.assertNotIn("[", meal["name"])

            price = meal["studentPrice"]
            self.assert_(isinstance(price, (int, float)), price)
            self.assert_(price > 0 or price == -1, price)

            self.assertIsInstance(meal["condiments"], list)
            for condiment in meal["condiments"]:
                self.assert_(condiment)

    @parameterized.expand(for_implementations(JsonCafeteria(), "2015-04-02"), testcase_func_name=implementation_name)
    def test_format_meal(self, implementation, day):
        with open(TEST_PATH + "/data/cafeteria/{}_day.txt".format(day)) as f:
            content = json.load(f)
        with open(TEST_PATH + "/data/cafeteria/{}_expected.txt".format(day)) as f:
            expected = json.load(f)["meals"]

        for meal in content:
            meal_format = implementation._format_meal(meal)
            self.assertIn(meal_format, expected)

    @parameterized.expand(for_implementations(JsonCafeteria()), testcase_func_name=implementation_name)
    def test_format_day(self, implementation):
        with open(TEST_PATH + "/data/cafeteria/meal_overview.txt") as f:
            content = json.load(f)
        with open(TEST_PATH + "/data/cafeteria/meal_overview_expected.txt") as f:
            expected = json.load(f)

        days = list(map(implementation._format_day, content))

        self.assertListEqual(days, expected)

    @parameterized.expand(for_implementations(HtmlCafeteria()), testcase_func_name=implementation_name)
    def test_format_meals(self, implementation):
        with open(TEST_PATH + "/data/cafeteria/kw21.txt") as f:
            soup = BeautifulSoup(f)
        with open(TEST_PATH + "/data/cafeteria/kw21_expected.txt") as f:
            expected = json.load(f)
            expected = [meal for day in expected for meal in day["meals"]]

        day_tables = soup.find_all("table", width="100%")
        self.assertGreater(len(day_tables), 0)

        for day_table in day_tables:

            self.assertGreater(len(day_table), 0)

            meals = implementation._format_meals(day_table)
            for meal in meals:
                self.assertIn(meal, expected, "\n" + pprint.pformat(meal, indent=1))

    @parameterized.expand(for_implementations(HtmlCafeteria()), testcase_func_name=implementation_name)
    def test_format_day(self, implementation):
        with open(TEST_PATH + "/data/cafeteria/kw21.txt") as f:
            soup = BeautifulSoup(f)
        with open(TEST_PATH + "/data/cafeteria/kw21_expected.txt") as f:
            expected = json.load(f)

        days = soup.find_all("h1")[1:]
        self.assertGreater(len(days), 0)

        results = []
        for day in days:
            day_text = day.text.split(" ")[1] + "2015"
            date = datetime.datetime.now().strptime(day_text, "%d.%m.%Y")

            day_result = implementation._format_day(day, date)
            self.assertGreater(len(day_result), 0)
            results.append(day_result)

        self.assertListEqual(results, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_open_days(self, implementation):
        if not hasattr(implementation, "get_open_days"):
            return

        with open(TEST_PATH + "/data/cafeteria/open.txt") as f:
            content = f.read()
        with open(TEST_PATH + "/data/cafeteria/open_expected.txt") as f:
            expected = json.load(f)
        with requests_mock.mock() as m:
            m.get("http://openmensa.org/api/v2/canteens/33/days/", text=content)
            open_days = implementation.get_open_days()

        self.assertEqual(open_days, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_open_days_error(self, implementation):
        if not hasattr(implementation, "get_open_days"):
            return

        with requests_mock.mock() as m:
            m.get("http://openmensa.org/api/v2/canteens/33/days/", status_code=500)

            with self.assertRaises(InvalidAPIUsage) as cm:
                implementation.get_open_days()
            self.assertEqual(cm.exception.status_code, 400)

    @parameterized.expand(for_implementations(factory.implementations))
    @attr('live')
    def test_get_open_days_live(self, implementation):
        if not hasattr(implementation, "get_open_days"):
            return

        open_days = implementation.get_open_days()

        self.assertIsInstance(open_days, list)
        self.assertGreaterEqual(len(open_days), 1, open_days)

        for day in open_days:
            datetime.datetime.strptime(day, "%Y-%m-%d")

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @mock.patch.object(AbstractCafeteria, '_now', _mock_now)
    def test_get_day_plan(self, implementation):
        if not hasattr(implementation, "get_day_plan"):
            return

        with open(TEST_PATH + "/data/cafeteria/2015-05-21_expected.txt") as f:
            expected = json.load(f)
        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/cafeteria/2015-05-21_day.txt") as f:
                m.get("http://openmensa.org/api/v2/canteens/33/days/2015-05-21/meals", text=f.read())
            with open(TEST_PATH + "/data/cafeteria/kw21.txt") as f:
                m.get("http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw=21", text=f.read())

            day = implementation.get_day_plan("2015-05-21")

        self.assertEqual(day, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_day_plan_error(self, implementation):
        if not hasattr(implementation, "get_day_plan"):
            return

        with requests_mock.mock() as m:
            m.get("http://openmensa.org/api/v2/canteens/33/days/2015-04-02/meals", status_code=404)
            m.get("http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw=14", status_code=404)

            day = implementation.get_day_plan("2015-04-02")

        self.assertDictEqual(day, {"date": "2015-04-02", "closed": True, "meals": []})

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_day_plan_live(self, implementation):
        if not hasattr(implementation, "get_day_plan"):
            return

        day = implementation.get_day_plan(self.now_format)

        self._check_day(day)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_day_plan_error(self, implementation):
        if not hasattr(implementation, "get_day_plan"):
            return

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day_plan("")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day_plan("2015-04")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day_plan("2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

    @parameterized.expand(for_implementations(HtmlCafeteria()), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_day_plan_error2(self, implementation):
        if not hasattr(implementation, "get_day_plan"):
            return

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_day_plan("2000-01-01")
        self.assertEqual(cm.exception.status_code, 400)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_week_plan(self, implementation):
        if not hasattr(implementation, "get_week_plan"):
            return

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/cafeteria/meal_overview.txt") as f:
                m.get("http://openmensa.org/api/v2/canteens/33/meals", text=f.read())
            with open(TEST_PATH + "/data/cafeteria/kw14.txt") as f:
                m.get("http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw=14", text=f.read())

            days = implementation.get_week_plan("2015-04-02")

        # days in the pasts are not shown in the list
        self.assertEqual(days, [])

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_week_plan2(self, implementation):
        if not hasattr(implementation, "get_week_plan"):
            return

        date = self.now + datetime.timedelta(days=60)
        week = str(date.isocalendar()[1])

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/cafeteria/meal_overview.txt") as f:
                m.get("http://openmensa.org/api/v2/canteens/33/meals", text=f.read())
            with open(TEST_PATH + "/data/cafeteria/kw14.txt") as f:
                m.get("http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw=" + week,
                      text=f.read())

            days = implementation.get_week_plan(date.strftime("%Y-%m-%d"))

        # days to far in the future can't be shown
        self.assertEqual(days, [])

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_week_plan_live(self, implementation):
        if not hasattr(implementation, "get_week_plan"):
            return

        days = implementation.get_week_plan(self.now_format)

        self.assertIsInstance(days, list)
        self.assertEqual(len(days), 7 - self.now.weekday())

        for day in days:
            self._check_day(day)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('error')
    def test_get_week_plan_error(self, implementation):
        if not hasattr(implementation, "get_week_plan"):
            return

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week_plan("")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week_plan("2015-04")
        self.assertEqual(cm.exception.status_code, 400)

        with self.assertRaises(InvalidAPIUsage) as cm:
            implementation.get_week_plan("2015-13-02")
        self.assertEqual(cm.exception.status_code, 400)

        with requests_mock.mock() as m:
            m.get("http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw=14", status_code=500)
            m.get("http://openmensa.org/api/v2/canteens/33/meals", status_code=500)

            with self.assertRaises(InvalidAPIUsage) as cm:
                implementation.get_week_plan("2015-04-02")
            self.assertEqual(cm.exception.status_code, 400)

if __name__ == '__main__':
    print(CafeteriaTestCase().test_format_date.__name__)
    unittest.main()