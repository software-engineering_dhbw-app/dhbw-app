# -*- coding: utf-8 -*-
import unittest
from io import open
import json

from nose.plugins.attrib import attr
from nose_parameterized import parameterized
import requests_mock

from flask.ext.app.model.factory import ContactFactory
from tests import FlaskAppTestCase, implementation_name, for_implementations, TEST_PATH


class ContactTestCase(FlaskAppTestCase):
    factory = ContactFactory

    def setUp(self):
        super(ContactTestCase, self).setUp()

        self.maxDiff = None

    def _check_person(self, person):
        self.assertIsInstance(person, dict)
        dict_keys = ['function', 'name', 'room', 'telephone']
        self.assertListEqual(sorted(person.keys()), dict_keys)

        for key in ['function', 'name', 'room']:
            self.assert_(person.get(key), person)

        if person.get("telephone"):
            self.assert_(person.get("telephone").startswith("+49 "), person)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_contact(self, implementation):
        if not hasattr(implementation, "get_contact"):
            pass

        with open(TEST_PATH + "/data/contact/contact_expected.txt") as f:
            expected = json.load(f)

        with requests_mock.mock() as m:
            with open(TEST_PATH + "/data/contact/website.txt") as f:
                m.get("https://www.dhbw-karlsruhe.de/allgemein/dhbw-karlsruhe/mitarbeiterverzeichnis/", text=f.read())

            people = implementation.get_contact()

        self.assertEqual(people, expected)

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    @attr('live')
    def test_get_contact_live(self, implementation):
        if not hasattr(implementation, "get_contact"):
            pass

        people = implementation.get_contact()

        self.assertGreater(len(people), 0)
        for person in people:
            self._check_person(person)


if __name__ == '__main__':
    unittest.main()