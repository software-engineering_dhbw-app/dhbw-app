# -*- coding: utf-8 -*-
import unittest

from nose_parameterized import parameterized

from flask.ext.app.model.factory import RoomsFactory
from tests import FlaskAppTestCase, for_implementations, implementation_name


class RoomsTestCase(FlaskAppTestCase):
    factory = RoomsFactory

    def setUp(self):
        super(RoomsTestCase, self).setUp()

        self.maxDiff = None

    def _check_room(self, room):
        self.assertIsInstance(room, dict)
        self.assertListEqual(sorted(room.keys()), ['description', 'floor', 'hallway', 'hallwayPosition', 'roomNr'])

        self.assert_(room["floor"] > 0 or room["floor"] == -1, room["floor"])

    @parameterized.expand(for_implementations(factory.implementations), testcase_func_name=implementation_name)
    def test_get_rooms(self, implementation):
        if not hasattr(implementation, "get_rooms"):
            return

        rooms = implementation.get_rooms()

        for room in rooms:
            self._check_room(room)


if __name__ == '__main__':
    unittest.main()