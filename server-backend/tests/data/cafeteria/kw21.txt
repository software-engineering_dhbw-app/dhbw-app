
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
Projektmanagement, Konzeption und Webdesign www.ultrabold.com
ultrabold Kommunikationsdesign GmbH, Mannheim
contact@ultrabold.com
http://www.ultrabold.com

Based on campus red content-management system by codepoetry
http://campus-red.de
-->
<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="content-language" content="de">
  <meta http-equiv="language" content="de">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <meta name="revisit-after" content="14 days">
  <meta name="robots" content="index,follow">
  <meta name="generator" content="plato cms - plato framework v1.8.2 - codepoetry.de">
  <meta http-equiv="imagetoolbar" content="no">
  <meta name="MSSmartTagsPreventParsing" content="true">
  <meta name="rating" content="Safe For Kids">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="copyright" content="">
  <meta name="description" content="Service für Studierende in Karlsruhe, Pforzheim, Bretten, Bruchsal und Calw: Wohnheimanträge, Bafög, Zimmervermittlung, Essen/Mensen">
  <meta name="keywords" content="Studentenwerk, Studierendenwerk, Karlsruhe, Pforzheim, BAföG, Darlehen, Wohnheime, Stipendien, Stipendium, Wohnen, Mensa, Mensen, Studentenwohnheime, campuslife, Zimmervermittlung, Tandem, Studium, Student, Studierende, Studentenhaus, PBS, Beratung">
  <meta name="author" content="Studierendenwerk Karlsruhe">

  <meta name="title" content="Studierendenwerk Karlsruhe">
  <meta name="google-site-verification" content="0pObDgX61tTl4yadjyTp7K07q1xKDHEbTDinpmaSnsg">
  <link rel="image_src" href="/layout/swka.gif">


  <script type="text/javascript" src="/javascript/jquery/jquery.pack.js"></script>
  <script type="text/javascript" src="/javascript/script.js"></script>
  <script type="text/javascript" src="/javascript/jquery/jqscript.js"></script>
  <script type="text/javascript" src="/javascript/flashdetection.js"></script>
      <script src="http://f1.eu.readspeaker.com/script/6606/ReadSpeaker.js?pids=embhl" type="text/javascript"></script>



  <title>Studierendenwerk Karlsruhe :: Essen </title>
    <link rel="stylesheet" type="text/css" href="/css/plato.css">
  <link rel="stylesheet" type="text/css" href="/css/navigation.css">
  <link rel="stylesheet" type="text/css" href="/css/style.css?v=20141205">

    <link rel="stylesheet" type="text/css" href="/css/essen.css">


    <style>
  body { background:none; }
  </style>

</head>
<body class="plato"><table cellpadding=5 cellspacing=0 width="100%"><tr><td width="100%">
<div id="platocontent" class="platocontent"><a id="content_91"></a><h1 style="color:#000;font-size:22px">Mensa Erzbergerstraße</h1>
Preise für Studenten<p>
<style type="text/css">
td,div,span {
font-size:20px !important;
line-height:48px !important;
color:#000 !important;
}
.platomargin {
display:none;
}
</style>








<br>
<h1 style="color:#000;font-size:22px">Mo 18.05.</h1>


    <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;border-color:#DDD;max-width: 1000px;">



    <tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 1</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Gemüsemaultaschen</b>

           <span>Schmorzwiebeln hausgemachter Kartoffelsalat</span>
          (3,Gl,Ei,So,Sn,ML,Se,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,50 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[RAT]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Rindergulasch</b>


          (Gl,So,Sn)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,20 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Hähnchenbrust im Knuspermantel</b>

           <span>Bratensoße</span>
          (1,3,Gl)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,10 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Pommes</b>


          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,90 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Petersilienkartoffeln</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Langkornreis</b>


          (1,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Broccolicremesuppe</b>


          (1,Gl,So,Sn,ML,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,45 &euro;</span>

                </td></tr>
        </table>

    </td></tr>



    <tr style="b"><td width="20%">Wahlessen 2</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Salatbuffet überwiegend vegetarisch</b>


          (1,3,4,7,8,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(pro 100g) 0,79 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[R]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Rindfleisch, Tomaten, Zucchini, Paprika in roter Thai-Soße</b>

           <span>Langkornreis</span>
          (1,2,7,Gl,So,Se,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">4,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Desserts</b>


          (1,Gl,Nu,Ei,So,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(ab) 0,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Beilagensalat</b>


          (Ei,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,75 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Blumenkohlgemüse</b>


          (1,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
        </table>

    </td></tr>
   </table>









<br>
<h1 style="color:#000;font-size:22px">Di 19.05.</h1>


    <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;border-color:#DDD;max-width: 1000px;">



    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 1</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Spaghetti mit Lachs, Tomaten und Majoran</b>


          (2,Gl,Ei,Fi,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">3,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[S]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Spießbraten</b>

           <span>Bratensoße</span>
          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,10 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Teigwaren</b>


          (Gl)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,73 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Kartoffelpüree</b>


          (1,3,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Tagessuppe</b>


          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,45 &euro;</span>

                </td></tr>
        </table>

    </td></tr>


    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 2</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Salatbuffet überwiegend vegetarisch</b>


          (1,3,4,7,8,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(pro 100g) 0,79 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Gebackener Hirtenkäse</b>

           <span>mit Tzatziki</span>
          (3,Gl,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,00 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Desserts</b>


          (1,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(ab) 0,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Beilagensalat</b>


          (Ei,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,75 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Karottenwedges</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
        </table>

    </td></tr>
   </table>









<br>
<h1 style="color:#000;font-size:22px">Mi 20.05.</h1>


    <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;border-color:#DDD;max-width: 1000px;">



    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 1</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[S]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Erbseneintopf,</b>

           <span>auf Wunsch mit Bockwurst oder vegetarischer Wurst, Baguettebrötchen</span>
          (4,5,Gl,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,50 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[RAT]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Rindergulasch</b>


          (Gl,So,Sn)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,30 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Spätzle</b>


          (1,Gl,Ei)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,90 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Langkornreis</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Tagessuppe</b>


          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,45 &euro;</span>

                </td></tr>
        </table>

    </td></tr>



    <tr style="b"><td width="20%">Wahlessen 2</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Salatbuffet überwiegend vegetarisch</b>


          (1,3,4,7,8,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(pro 100g) 0,79 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Getreide-Käse-Krusties</b>

           <span>Romanobohnengemüse</span>
          (1,Gl,Sn,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">1,90 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Desserts</b>


          (1,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(ab) 0,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Beilagensalat</b>


          (Ei,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,75 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Kaisergemüse</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
        </table>

    </td></tr>
   </table>









<br>
<h1 style="color:#000;font-size:22px">Do 21.05.</h1>


    <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;border-color:#DDD;max-width: 1000px;">



    <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 1</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Portion Stangenspargel</b>

           <span>dazu Hollandaise oder zerlassene Butter mit Pfannkuchen oder Kartoffeln</span>
          (Gl,Ei,ML,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">5,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[S]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>3 Frikadellen</b>

           <span>grober Dijonsenfsoße</span>
          (Gl,Ei)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">1,80 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Teigwaren</b>


          (Gl)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,73 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Petersilienkartoffeln</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Tagessuppe</b>


          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,45 &euro;</span>

                </td></tr>
        </table>

    </td></tr>


    <tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 2</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Salatbuffet überwiegend vegetarisch</b>


          (1,3,4,7,8,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(pro 100g) 0,79 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>3 Semmelknödel mit frischen Rahmchampignons</b>


          (1,Gl,Ei,ML,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,25 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Desserts</b>


          (1,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(ab) 0,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Beilagensalat</b>


          (Ei,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,75 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Balkangemüse</b>


          (1,Gl,So,Sn)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
        </table>

    </td></tr>
   </table>









<br>
<h1 style="color:#000;font-size:22px">Fr 22.05.</h1>


    <table cellspacing="0" cellpadding="5" border="1" width="100%" style="border-collapse:collapse;border-color:#DDD;max-width: 1000px;">




    <tr style="b"><td width="20%">Wahlessen 1</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[RAT]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Mie Nudeln mit Gemüse ToGo</b>

           <span>mit Geflügelstreifen oder Mie-Nudeln mit würzigem Hackfleisch</span>
          (2,Gl,So,Sn,Sa)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,55 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Mie Nudeln mit Gemüse ToGo</b>


          (2,Gl,So,Sn,Sa)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">2,35 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Frühlingsrolle mit Hühnerfleisch</b>

           <span>Sweet Chilli Soße</span>
          (2,3,Gl,Ei,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">1,90 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Petersilienkartoffeln</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Langkornreis</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top"></td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Tagessuppe</b>


          </span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,45 &euro;</span>

                </td></tr>
        </table>

    </td></tr>


    <tr></tr><tr></tr><tr></tr><tr></tr>
    <tr style="b"><td width="20%">Wahlessen 2</td><td>

        <table cellspacing="0" cellpadding="0" border="0" class="easy-tab-dot" >

                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Salatbuffet überwiegend vegetarisch</b>


          (1,3,4,7,8,ML,Sf)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(pro 100g) 0,79 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Fagottini</b>

           <span>Tomaten-Rahmsoße Reibekäse</span>
          (1,Gl,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">1,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG,LAB]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Desserts</b>


          (1,ML)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">(ab) 0,95 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VEG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Beilagensalat</b>


          (Ei,So,Sn,Se)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,75 &euro;</span>

                </td></tr>
                  <tr class="handicap">
            <td style="background: white;width:115px;" valign="top">[VG]</td>
            <td class="first dot" valign="top" ><span class="bg">
            <b>Mischgemüse</b>


          (1)</span>
          </td><td style="text-align: right;vertical-align:bottom;;white-space: nowrap;" >
          <span class="bg">0,70 &euro;</span>

                </td></tr>
        </table>

    </td></tr>
   </table>



<br>
<b>Legende Speisezusätze:</b><br>(1) mit Farbstoff <br> (2) mit Konservierungsstoff <br> (3) mit Antioxidationsmittel<br>(4) mit Geschmacksverstärker <br> (5) mit Phosphat <br> (6) Oberfläche gewachst<br>(7) geschwefelt <br> (8) Oliven geschwärzt <br> (9) mit Süßungsmittel<br>(10) kann bei übermäßigem Verzehr abführend wirken<br>(11) enthält eine Phenylalaninquelle <br> (12) kann Restalkohol enthalten<br>(13) Kennzeichnung siehe Tageskarte
<br>
(B)kontrolliert biologischer Anbau / DE007 Öko Kontrollstelle<br>(VEG)vegetarisches Gericht <br>(ohne Fleischzusatz)<br>(VG)veganes Gericht<br>(S)enthält Schweinefleisch<br>(MSC)MSC-zertifizierter Fisch<br>(R)enthält Rindfleisch<br>(RAT)enthält Rindfleisch aus artgerechter Tierhaltung<br>(LAB)mit tierischem Lab<br>(Gl)Glutenhaltiges Getreide<br>(Nu)Schalenfrüchte / Nüsse<br>(Ei)Eier<br>(Er)Erdnüsse<br>(So)Soja<br>(Sn)Senf<br>(Kr)Krebstiere<br>(Fi)Fisch<br>(ML)Milch / Laktose<br>(Se)Sellerie<br>(Sf)Schwefeldioxid / Sulfit<br>(Sa)Sesam<br>(Lu)Lupine<br>(We)Weichtiere</div>

</td><td valign="top" width="200px" nowrap="nowrap" style="padding-left:18px"><div id="platomargin" class="platomargin"><a id="margin_75"></a><div class="content-block"><div class="content-wrapper"><strong>Anregungen &amp; Feedback</strong><br/>Falls Sie Anregungen haben oder es Ihnen einmal nicht geschmeckt hat: <br/>Wir sind gerne für Sie da! <br/>Sagen Sie uns Ihre Meinung - Sie helfen uns damit, uns zu verbessern.<p/>Einfach Mail senden an: <a class="mail-link" href="mailto:gastronomie@sw-ka.de">gastronomie@sw-ka.de</a></div></div><p class="p-block"></p></div>

</td></tr>
</table>
</body></html>