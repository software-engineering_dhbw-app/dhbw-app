# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import unittest
import inspect

from flask.ext.app.model.cafeteria import HtmlCafeteria, JsonCafeteria
import flask_app


TEST_PATH = os.path.abspath(os.path.dirname(__file__))


def for_implementations(*args):
    implementations = args[0] if isinstance(args[0], list) else [args[0]]

    return [[impl] + list(args[1:]) for impl in implementations]


def implementation_name(testcase_func, param_num, params):
    class_ = params.args[0]
    class_ = class_.__name__ if inspect.isclass(class_) else type(class_).__name__
    return testcase_func.__name__ + "_" + class_


class GeneralTestCase(unittest.TestCase):
    def test_for_implementations(self):
        self.assertListEqual(for_implementations("1", "3"), [["1", "3"]])
        self.assertListEqual(for_implementations(["1", "3"]), [["1"], ["3"]])
        self.assertListEqual(for_implementations(["1", "3"], "4"), [["1", "4"], ["3", "4"]])

        implementation_result = for_implementations([JsonCafeteria, HtmlCafeteria])
        self.assertListEqual(implementation_result, [[JsonCafeteria], [HtmlCafeteria]])


class FlaskAppTestCase(unittest.TestCase):

    factory = None

    def setUp(self):
        os.environ['MODE'] = 'TESTING'
        self.app = flask_app.create_app()
        self.client = self.app.test_client()
