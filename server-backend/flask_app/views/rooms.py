#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import RoomsFactory
from . import convert_to_seconds, get_endpoint, make_response

blueprint = Blueprint('rooms', __name__, url_prefix=get_endpoint("rooms/"))


@blueprint.route('/')
@cache.cached(convert_to_seconds(days=7))
def dhbw_news():
    u"""
    Show an overview of all rooms of DHBW Karlsruhe.

    **Example request**:

    .. sourcecode:: http

        GET /v2/rooms/ HTTP/1.1
        Accept: application/json

    **Example response** (truncated):

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "hallwayPosition": "Gangende",
          "hallway": "Südflügel",
          "roomNr": "A173",
          "description": "Hörsaal",
          "floor": 1
         },
         {
          "hallwayPosition": "Ganganfang",
          "hallway": "Südflügel",
          "roomNr": "A262",
          "description": "Hörsaal",
          "floor": 2
         },
         {
          "hallwayPosition": "Gangende",
          "hallway": "Südflügel",
          "roomNr": "A369",
          "description": "Hörsaal",
          "floor": 3
         },
         {
          "hallwayPosition": "Gangmitte",
          "hallway": "Südflügel",
          "roomNr": "A169",
          "description": "",
          "floor": 1
         },
        ]

    :>jsonarr str hallwayPosition: The section of the hallway where the room is.
    :>jsonarr str hallway: The section of the building where the hallway is.
    :>jsonarr str roomNr: The number of the room.
    :>jsonarr str description: The function of the room.
    :>jsonarr str description: The floor this room is on. Will return `-1` if that information isn't provided.
    """

    return make_response(RoomsFactory.get_rooms())