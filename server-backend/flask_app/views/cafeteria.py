# -*- coding: utf-8 -*-
import datetime

from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import CafeteriaFactory
from . import convert_to_seconds, get_endpoint, make_response


__author__ = 'jhadleyjack'

blueprint = Blueprint('cafeteria', __name__, url_prefix=get_endpoint("cafeteria/"))


@blueprint.route("/")
@cache.cached(convert_to_seconds(days=1))
def cafeteria_get_open_days():
    u"""
    TODO
    """

    return make_response(CafeteriaFactory.get_open_days())


@blueprint.route('day/')
def cafeteria_today():
    u"""
    Meals that are served today in the cafeteria. If no meals are served today,
    then an empty list will be returned.

    **Example request**:

    .. sourcecode:: http

        GET /v2/cafeteria/day/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "date": "2015-04-13",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.55,
            "name": "Mie-Nudeln mit Schweinefilet, Gemüse in Thai-Sauce Mie-Nudeln",
            "condiments": [
             "1",
             "2",
             "Gl",
             "So"
            ]
           },
           {
            "category": "Wahlessen 1",
            "studentPrice": -1,
            "name": "Portion Stangenspargel dazu Hollandaise",
            "condiments": [
             "Ei",
             "Gl",
             "ML",
             "Se"
            ]
           }
          ...
        ]

    :>json str date: The date of the current day.
    :>json boolean closed: If the cafeteria is closed today.
    :>json list[json] meals: A list of all the meals that are available today.
      Can be empty if no meals are available.
    :>jsonarr str name: The full name of the meal (can also contain the subheading
      for the meal if available).
    :>jsonarr int studentPrice: The price that students have to pay. Can be `-1`
      if no price was provided.
    :>jsonarr list[str] condiments: A list of condiment properties. The meaning
      of the numbers is defined on the `Studentenwerk Karlsruhe website
      <http://www.sw-ka.de/en/essen/>`_ (i.e. with preserving agents).
    :>jsonarr str category: The category the meal belongs to
      ("Wahlessen 1" or "Wahlessen 2")
    """

    return cafeteria_day(datetime.date.today().isoformat())


@blueprint.route('day/<day>/')
@cache.cached(convert_to_seconds(hours=1))
def cafeteria_day(day):
    u"""
    Meals that are served on the selected day in the cafeteria.
    If no meals are served on that day then an empty list will be returned.

    **Example request**:

    .. sourcecode:: http

        GET /v2/cafeteria/day/2015-04-13/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "date": "2015-04-13",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.55,
            "name": "Mie-Nudeln mit Schweinefilet, Gemüse in Thai-Sauce",
            "condiments": [
             "1",
             "2",
             "Gl",
             "So"
            ]
           },
           {
            "category": "Wahlessen 1",
            "studentPrice": -1,
            "name": "Portion Stangenspargel dazu Hollandaise",
            "condiments": [
             "Ei",
             "Gl",
             "ML",
             "Se"
            ]
           }
          ...
        ]

    :param str day: The day for which the information should be shown.
    :>json str date: The date of that day.
    :>json boolean closed: If the cafeteria is closed on that day.
    :>json list[json] meals: A list of all the meals that are available on that day.
      Can be empty if no meals are available.
    :>jsonarr str name: The full name of the meal (can also contain the subheading
      for the meal if available).
    :>jsonarr int studentPrice: The price that students have to pay. Can be `-1`
      if no price was provided.
    :>jsonarr list[str] condiments: A list of condiment properties. The meaning
      of the numbers is defined on the `Studentenwerk Karlsruhe website
      <http://www.sw-ka.de/en/essen/>`_ (i.e. with preserving agents).
    :>jsonarr str category: The category the meal belongs to
      ("Wahlessen 1" or "Wahlessen 2")
    """
    return make_response(CafeteriaFactory.get_day_plan(day))


@blueprint.route('week/')
def cafeteria_this_week():
    u"""
    Meals that are served this week in the cafeteria.

    **Example request**:

    .. sourcecode:: http

        GET /v2/cafeteria/day/ HTTP/1.1
        Accept: application/json

    **Example response** (truncated):

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "date": "2015-04-13",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.55,
            "name": "Mie-Nudeln mit Schweinefilet, Gemüse in Thai-Sauce",
            "condiments": [
             "1",
             "2",
             "Gl",
             "So"
            ]
           },
           {
            "category": "Wahlessen 1",
            "studentPrice": -1,
            "name": "Portion Stangenspargel dazu Hollandaise",
            "condiments": [
             "Ei",
             "Gl",
             "ML",
             "Se"
            ]
           }
         },
         {
          "date": "2015-04-14",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.7,
            "name": "SchniPoSa / SteakPoSa",
            "condiments": [
             "Ei",
             "Gl",
             "Sn"
            ]
           }
        ]


    Die Response enthält eine Liste von Objekten für die gilt:

    :>jsonarr str date: The date for the day.
    :>jsonarr boolean closed: If the cafeteria is closed on that day.
    :>jsonarr list[json] meals: A list of all the meals that are available today.
      Can be empty if no meals are available.

    Jedes von den Objekten hat unter `meals` eine Liste für die gilt:

    :>jsonarr str name: The full name of the meal (can also contain the subheading
      for the meal if available).
    :>jsonarr int studentPrice: The price that students have to pay. Can be `-1`
      if no price was provided.
    :>jsonarr list[str] condiments: A list of condiment properties. The meaning
      of the numbers is defined on the `Studentenwerk Karlsruhe website
      <http://www.sw-ka.de/en/essen/>`_ (i.e. with preserving agents).
    :>jsonarr str category: The category the meal belongs to
      ("Wahlessen 1" or "Wahlessen 2")
    """

    return cafeteria_week(datetime.date.today().isoformat())


@blueprint.route('week/<day>/')
@cache.cached(convert_to_seconds(hours=1))
def cafeteria_week(day):
    u"""
    Meals that are served on the selected week in the cafeteria.

    **Example request**:

    .. sourcecode:: http

        GET /v2/cafeteria/day/2015-04-14/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "date": "2015-04-13",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.55,
            "name": "Mie-Nudeln mit Schweinefilet, Gemüse in Thai-Sauce",
            "condiments": [
             "1",
             "2",
             "Gl",
             "So"
            ]
           },
           {
            "category": "Wahlessen 1",
            "studentPrice": -1,
            "name": "Portion Stangenspargel dazu Hollandaise",
            "condiments": [
             "Ei",
             "Gl",
             "ML",
             "Se"
            ]
           }

          ...

          {
          "date": "2015-04-14",
          "closed": false,
          "meals": [
           {
            "category": "Wahlessen 1",
            "studentPrice": 2.7,
            "name": "SchniPoSa / SteakPoSa",
            "condiments": [
             "Ei",
             "Gl",
             "Sn"
            ]
           },

           ...
        ]

    :param str day: One day which is in the wanted week. This day doesn't have
      to be the Monday, it can be any day of the week.

    Die Response enthält eine Liste von Objekten für die gilt:

    :>jsonarr str date: The date for the day.
    :>jsonarr boolean closed: If the cafeteria is closed on that day.
    :>jsonarr list[json] meals: A list of all the meals that are available today.
      Can be empty if no meals are available.

    Jedes von den Objekten hat unter `meals` eine Liste für die gilt:

    :>jsonarr str name: The full name of the meal (can also contain the subheading
      for the meal if available).
    :>jsonarr int studentPrice: The price that students have to pay. Can be `-1`
      if no price was provided.
    :>jsonarr list[str] condiments: A list of condiment properties. The meaning
      of the numbers is defined on the `Studentenwerk Karlsruhe website
      <http://www.sw-ka.de/en/essen/>`_ (i.e. with preserving agents).
    :>jsonarr str category: The category the meal belongs to ("Wahlessen 1" or "Wahlessen 2")
    """
    return make_response(CafeteriaFactory.get_week_plan(day))
