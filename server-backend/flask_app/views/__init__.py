# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json
import datetime

from flask import Response, request

from .. import __version__


def get_endpoint(url):
    return "/{}/{}".format(__version__, url)


def make_response(content):
    # sort_keys=True?
    result = json.dumps(content, indent=1, ensure_ascii=False).encode('utf8')
    response = Response(result, content_type='application/json;charset=utf-8')

    # Caution: the API in the current implementation is read-only
    # should this change in the future, you probably should disable this setting
    response.headers['Access-Control-Allow-Origin'] = '*'

    return response


def request_wants_json():
    best = request.accept_mimetypes.best_match(['application/json', 'text/html'])
    mimetypes = request.accept_mimetypes
    return best == 'application/json' and mimetypes[best] > mimetypes['text/html']


def convert_to_seconds(days=0, hours=0, minutes=0, seconds=0):
    delta = datetime.timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds)
    return int(delta.total_seconds())


def make_cache_key(*args, **kwargs):
    # hash(request.args) has probably the same result, but didn't find any information on it
    return 'view/{0}/{1}'.format(request.path, hash(frozenset(request.args.items())))
