# -*- coding: utf-8 -*-
from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import ContactFactory
from . import convert_to_seconds, get_endpoint, make_response


blueprint = Blueprint('contact', __name__, url_prefix=get_endpoint("contact/"))


@blueprint.route('/')
@cache.cached(convert_to_seconds(days=1))
def dhbw_contact():
    u"""
    Shows the list of contact persons found on the DHBW website.

    **Example request**:

    .. sourcecode:: http

        GET /v2/contact/ HTTP/1.1
        Host: hadleyjack.de
        Accept: application/json

    **Example response** (truncated):

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "telephone": "+49 721 9735-732",
          "function": "Personalverwaltung",
          "name": "Agrawal, Anja",
          "room": "B570.8"
         },
         {
          "telephone": "+49 721 9735-804",
          "function": "Sekretariat Elektrotechnik",
          "name": "Ahrens, Britta",
          "room": "E512"
         },
         {
          "telephone": "+49 721 9735-812",
          "function": "Sekretariat Maschinenbau",
          "name": "Ahrens, Ulrike",
          "room": "E512"
         }
        ]

    :>jsonarr str telephone: Telephone number with country code.
    :>jsonarr str function: The function title of the person.
    :>jsonarr str name: Name of the person in the format: Last Name, First Name.
    :>jsonarr str room: The room number.
    :status 200: Returned data successfully.
    :status 500: There was an error trying to retrieve the data for the response.
    """

    return make_response(ContactFactory.get_contact())
