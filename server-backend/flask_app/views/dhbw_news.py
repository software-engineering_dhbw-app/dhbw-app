# -*- coding: utf-8 -*-
from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import DhbwNewsFactory
from . import convert_to_seconds, get_endpoint, make_response


blueprint = Blueprint('dhbw_news', __name__, url_prefix=get_endpoint("dhbw-news/"))


@blueprint.route('/')
@cache.cached(convert_to_seconds(hours=1))
def dhbw_news():
    u"""
    Shows the news articles from the DHBW website.

    **Example request**:

    .. sourcecode:: http

        GET /v2/dhbw-news/ HTTP/1.1
        Accept: application/json

    **Example response** (truncated):

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "content": "<p style=\"margin: 0cm 0cm 10pt;\">Maschinenbau, Wirtschaft...",
          "published": "2015-04-08T10:49:00+02:00",
          "link": "https://www.dhbw-karlsruhe.de/nc/allgemein/newssingle/article/...",
          "description": "Orientierungsveranstaltung, Dienstag 14. April, 18.00...",
          "title": "Welche Hochschule passt zu mir"
         },
         {
          "content": "<b><span lang=\"EN-US\">“If the mind is the hardware,...",
          "published": "2015-04-08T10:25:00+02:00",
          "link": "https://www.dhbw-karlsruhe.de/nc/allgemein/newssingle/article/sprachenzentrum/",
          "description": "„Interkulturelles Training“, dienstags, ab 14. April...",
          "title": "Sprachenzentrum"
         }
        ]

    :>jsonarr str content: The content of the article.
    :>jsonarr str published: The time the article was published.
    :>jsonarr str link: A link to the article on the website.
    :>jsonarr str description: A short description of the article.
    :>jsonarr str title: The title of the article.
    :status 200: Returned data successfully.
    :status 500: There was an error trying to retrieve the data for the response.
    """

    return make_response(DhbwNewsFactory.get_news())
