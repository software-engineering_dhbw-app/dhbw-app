# -*- coding: utf-8 -*-
import datetime

from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import CalendarFactory
from . import get_endpoint, make_response, convert_to_seconds


blueprint = Blueprint('calendar', __name__, url_prefix=get_endpoint("calendar/"))


@blueprint.route('calendar/')
def calendar_all_courses():
    u"""This is a work in progress."""

    return make_response(CalendarFactory.get_all_courses())


# @blueprint.route(get_endpoint('calendar/<course>/'))
@blueprint.route('<course>/day/')
def calendar_today(course):
    u"""
    Shows all events which can be found for today in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/day/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Prüfung",
          "course": [
           "TINF13B1",
           "TINF13B2",
           "TINF13B4"
          ],
          "endTime": "2015-04-13T09:30:00",
          "lastModified": "2015-03-17T09:19:24+00:00",
          "location": "D221 T Hörsaal",
          "person": "Föll, Peter",
          "startTime": "2015-04-13T08:00:00",
          "title": "Nachklausur Formale Sprachen  (90 min)"
         },
         {
          "category": "Lehrveranstaltung",
          "course": [
           "TINF13B2"
          ],
          "endTime": "2015-04-13T11:45:00+00:00",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "location": "E209 INF Hörsaal",
          "person": "Berkling, Kay Margarethe",
          "startTime": "2015-04-13T08:30:00+00:00",
          "title": "Software Engineering"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return calendar_day(course, datetime.date.today().isoformat())


@blueprint.route('<course>/week/')
def calendar_this_week(course):
    u"""Shows all events which can be found for this week in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/week/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Lehrveranstaltung",
          "person": "Eisenbiegler, Jörn",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-14T09:00:00+00:00",
          "title": "Webengineering II",
          "lastModified": "2015-03-10T08:21:32+00:00",
          "endTime": "2015-04-14T12:15:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-14T11:30:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-14T13:00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Klages, Henner",
          "course": [
           "TINF13B1",
           "TINF13B2",
           "TINF13B4"
          ],
          "location": "C236 INF Hörsaal",
          "startTime": "2015-04-14T15:45:00",
          "title": "AdA (Z)",
          "lastModified": "2015-02-23T07:38:59+00:00",
          "endTime": "2015-04-14T18:15:00"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return calendar_week(course, datetime.date.today().isoformat())


@blueprint.route('<course>/month/')
@cache.cached(convert_to_seconds(hours=1))
def calendar_this_month(course):
    u"""Shows all events which can be found for this month in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/month/2015-04-14 HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Lehrveranstaltung",
          "person": "Eisenbiegler, Jörn",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T09:00:00+00:00",
          "title": "Webengineering II",
          "lastModified": "2015-03-10T08:21:32+00:00",
          "endTime": "2015-04-21T12:15:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T13:00:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-21T14:30:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-20T08:30:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-20T11:45:00+00:00"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return calendar_month(course, datetime.date.today().isoformat())


@blueprint.route('<course>/day/<day>/')
@cache.cached(convert_to_seconds(hours=1))
def calendar_day(course, day):
    u"""Shows all events which can be found for the selected day in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/day/2015-04-14 HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Lehrveranstaltung",
          "person": "Eisenbiegler, Jörn",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-14T09:00:00+00:00",
          "title": "Webengineering II",
          "lastModified": "2015-03-10T08:21:32+00:00",
          "endTime": "2015-04-14T12:15:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-14T11:30:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-14T13:00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Klages, Henner",
          "course": [
           "TINF13B1",
           "TINF13B2",
           "TINF13B4"
          ],
          "location": "C236 INF Hörsaal",
          "startTime": "2015-04-14T15:45:00",
          "title": "AdA (Z)",
          "lastModified": "2015-02-23T07:38:59+00:00",
          "endTime": "2015-04-14T18:15:00"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return make_response(CalendarFactory.get_day(course, day))


@blueprint.route('<course>/week/<day>/')
@cache.cached(convert_to_seconds(hours=1))
def calendar_week(course, day):
    u"""Shows all events which can be found for the selected week in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/week/2015-04-14/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Lehrveranstaltung",
          "person": "Eisenbiegler, Jörn",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T09:00:00+00:00",
          "title": "Webengineering II",
          "lastModified": "2015-03-10T08:21:32+00:00",
          "endTime": "2015-04-21T12:15:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T13:00:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-21T14:30:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-20T08:30:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-20T11:45:00+00:00"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :param str day: One day which is in the wanted week. This day doesn't have to be the Monday,
      it can be any day of the week.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return make_response(CalendarFactory.get_week(course, day))


@blueprint.route('<course>/month/<day>/')
@cache.cached(convert_to_seconds(hours=1))
def calendar_month(course, day):
    u"""Shows all events which can be found for the selected month in the course calendar.

    **Example request**:

    .. sourcecode:: http

        GET /v2/calendar/TINF13B2/month/2015-04-14 HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

       HTTP/1.1 200 OK
       Content-Type: application/json;charset=utf-8

        [
         {
          "category": "Lehrveranstaltung",
          "person": "Eisenbiegler, Jörn",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T09:00:00+00:00",
          "title": "Webengineering II",
          "lastModified": "2015-03-10T08:21:32+00:00",
          "endTime": "2015-04-21T12:15:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-21T13:00:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-21T14:30:00+00:00"
         },
         {
          "category": "Lehrveranstaltung",
          "person": "Berkling, Kay Margarethe",
          "course": [
           "TINF13B2"
          ],
          "location": "E209 INF Hörsaal",
          "startTime": "2015-04-20T08:30:00+00:00",
          "title": "Software Engineering",
          "lastModified": "2015-03-30T06:11:05+00:00",
          "endTime": "2015-04-20T11:45:00+00:00"
         }
        ]

    :param str course: The course for which the calendar should be used.
    :param str day: One day which is in the wanted month. This day doesn't have
      to be the first day of the month, it can be any day of the month.
    :>jsonarr str category: The category for the event.
    :>jsonarr str person: The person who is holding the event.
    :>jsonarr list[str] course: A list of courses this event applies to.
    :>jsonarr str location: The location where the event is held.
    :>jsonarr str startTime: The time the event starts.
    :>jsonarr str title: The title of the event.
    :>jsonarr str lastModified: The last time some details of this event has been changed.
    :>jsonarr str endTime: The time this event finishes.
    """

    return make_response(CalendarFactory.get_month(course, day))
