# -*- coding: utf-8 -*-
from flask import request, Blueprint

from ..utils.cache import cache
from ..custom_exceptions.api_exeption import InvalidAPIUsage
from ..model.factory import TransportationFactory
from . import convert_to_seconds, get_endpoint, make_response, make_cache_key


blueprint = Blueprint('transportation', __name__, url_prefix=get_endpoint("transportation/"))


@blueprint.route('/')
@cache.cached(convert_to_seconds(minutes=1))
def transport_next():
    u"""
    Shows the next few trains leaving towards the city center.

    **Example request**:

    .. sourcecode:: http

        GET /v2/transportation/ HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "line": "3",
          "destination": "Hauptbahnhof",
          "lineColor": "#9a763a",
          "time": "2015-04-13T19:00:51+02:00"
         },
         {
          "line": "3",
          "destination": "Hauptbahnhof",
          "lineColor": "#9a763a",
          "time": "2015-04-13T19:12:00+02:00"
         },
         {
          "line": "E",
          "destination": "Rheinhafen",
          "lineColor": "#a4a4a4",
          "time": "2015-04-13T19:15:00+02:00"
         },
         {
          "line": "3",
          "destination": "Hauptbahnhof",
          "lineColor": "#9a763a",
          "time": "2015-04-13T19:27:00+02:00"
         },
         {
          "line": "E",
          "destination": "Rheinhafen",
          "lineColor": "#a4a4a4",
          "time": "2015-04-13T19:35:00+02:00"
         }
        ]

    :>jsonarr str line: The line number.
    :>jsonarr str destination: The destination of the line.
    :>jsonarr str lineColor: The hexideximal triplets that represent
      the line color (i.e. '#FF0000').
    :>jsonarr str time: The time that the train arrives.
    :status 200: Returned data successfully.
    :status 500: There was an error trying to retrieve the data for the response.
    """

    return make_response(TransportationFactory.get_next())


@blueprint.route('search')
@cache.cached(convert_to_seconds(hours=1))
def transport_search():
    u"""

    .. warning::

        This function is still a work in progress.

    Shows the next few trains leaving towards the city center.

    **Example request with name search**:

    .. sourcecode:: http

        GET /v2/transportation/search?name=Duale%20Hocschule HTTP/1.1
        Accept: application/json

    **Example response with name search**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "id": "de:8212:12",
          "name": "Karlsruhe Duale Hochschule"
         },
         {
          "id": "de:8212:7003",
          "name": "Karlsruhe Kunstak./Hochschule"
         },
         {
          "id": "de:8215:37211",
          "name": "Ettlingen Volkshochschule"
         },
         {
          "id": "de:8215:37223",
          "name": "Gochsheim Kirche/Schule"
         },
         {
          "id": "de:8212:16",
          "name": "Durlach Friedrichschule"
         },
         {
          "id": "de:8216:12018",
          "name": "Bühlertal Josef-Schofer Schule"
         },
         {
          "id": "de:8215:8309",
          "name": "Malsch Hans-Thoma-Schule"
         }
        ]

    :>jsonarr str line: The line number.
    :>jsonarr str destination: The destination of the line.
    :>jsonarr str lineColor: The hexideximal triplets that represent
      the line color (i.e. '#FF0000').
    :>jsonarr str time: The time that the train arrives.
    :status 200: Returned data successfully.
    :status 500: There was an error trying to retrieve the data for the response.

    **Example request with lat/lon search**:

    .. sourcecode:: http

        GET /v2/transportation/search?lat=49.0246632&lon=8.3866504 HTTP/1.1
        Accept: application/json

    **Example response with lat/lon search**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
         {
          "distance": 253,
          "id": "de:8212:12",
          "name": "Karlsruhe Duale Hochschule"
         },
         {
          "distance": 276,
          "id": "de:8212:8",
          "name": "Karlsruhe Lilienthalstraße"
         },
         {
          "distance": 741,
          "id": "de:8212:7009",
          "name": "Karlsruhe Heidehof"
         },
         {
          "distance": 744,
          "id": "de:8212:7303",
          "name": "Karlsruhe Schützenhaus"
         },
         {
          "distance": 775,
          "id": "de:8212:5",
          "name": "Karlsruhe Synagoge"
         }
        ]
    """

    name = request.args.get('name')
    lat = request.args.get('lat')
    lon = request.args.get('lon')

    if name:
        return make_response(TransportationFactory.search_by_name(name))
    elif lat and lon:
        return make_response(TransportationFactory.search_by_location(lat, lon))
    else:
        raise InvalidAPIUsage(110, "Missing HTTP parameter: name or lat and lon.")
transport_search.make_cache_key = make_cache_key
