# -*- coding: utf-8 -*-
from flask import Blueprint

from ..utils.cache import cache
from ..model.factory import StuvNewsFactory
from . import convert_to_seconds, get_endpoint, make_response


blueprint = Blueprint('stuv_news', __name__, url_prefix=get_endpoint("stuv-news/"))


@blueprint.route('/')
@cache.cached(convert_to_seconds(hours=1))
def stuv_news():
    u"""
    Shows the news articles from the StuV Facebook page.

    **Example request**:

    .. sourcecode:: http

        GET /v2/stuv-news/ HTTP/1.1
        Accept: application/json

    **Example response** (truncated):

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json;charset=utf-8

        [
          {
          "content": "++++ Studium generale: Business-Knigge ++++<br />...",
          "published": "2015-04-10T16:01:00+01:00",
          "link": "http://www.facebook.com/stuv.dhbw.karlsruhe/posts/964058116960052",
          "title": "++++ Studium generale: Business-Knigge ++++Ihr stellt euch..."
         },
         {
          "content": "Bei schönem Wetter eure studentischen Interessen...",
          "published": "2015-04-10T15:00:25+01:00",
          "link": "http://www.facebook.com/stuv.dhbw.karlsruhe/photos/a.563549063677628...",
          "title": "Bei sch&#xf6;nem Wetter eure studentischen Interessen..."
         }
        ]

    :>jsonarr str content: The content of the article.
    :>jsonarr str published: The time the article was published.
    :>jsonarr str link: Link to the article on the website.
<<<<<<< HEAD
    :>jsonarr str title: The title of the article. Articles without title will be
      filtered out and not shown.
=======
    :>jsonarr str title: The title of the article. Articles without title will be filtered out and not shown.
>>>>>>> remotes/origin/master
    """

    return make_response(StuvNewsFactory.get_news())
