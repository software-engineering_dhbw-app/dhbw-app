__author__ = 'jhadleyjack'


class InvalidAPIUsage(Exception):
    status_dict = {}

    def __init__(self, error_code, message, status_code=None, payload=None):
        super(InvalidAPIUsage, self).__init__()
        self.error_code = error_code
        self.message = message
        if isinstance(status_code, basestring) and status_code in self.status_dict:
            self.status_code = self.status_dict[status_code]
        elif isinstance(status_code, int):
            self.status_code = status_code
        else:
            self.status_code = 400
        self.status_code = status_code if status_code is not None else 400
        self.payload = payload

    def to_dict(self):
        rv = self.payload or dict()
        rv['message'] = self.message
        rv['errorCode'] = self.error_code
        rv['statusCode'] = self.status_code
        rv['reference'] = "http://dhbw-app.readthedocs.org/en/latest/#document-content/error"
        return rv

    def __str__(self):
        return str(self.error_code) + ": " +self.message


class ExceptionManager(object):

    @classmethod
    def _format_arguments(cls, **kwargs):
        return ": " + ", ".join("{0} (given: {1})".format(key, value) for key, value in kwargs.items())

    @classmethod
    def invalid_argument(cls, error_code, _type, given, expected=None):
        expected_text = "expected: {},".format(expected) if expected else ""
        message_template = "The information provided for {} is in the wrong format ({}given: {})."
        return InvalidAPIUsage(error_code, message_template.format(_type, expected_text, given), 400)

    @classmethod
    def missing_argument(cls, error_code, *args):
        if len(args):
            message_template = "One argument is missing: {}."
        else:
            message_template = "Some arguments are missing: {}."
        return InvalidAPIUsage(error_code, message_template.format(", ".join(args)), 400)

    @classmethod
    def parse_error(cls, error_code, extra_text="", **kwargs):
        message_template = "Had an internal error with the given information{}. {}"
        return InvalidAPIUsage(error_code, message_template.format(cls._format_arguments(**kwargs), extra_text), 400)