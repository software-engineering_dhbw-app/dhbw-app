# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
import datetime
import calendar
from operator import itemgetter

from bs4 import BeautifulSoup
from dateutil import parser, tz
import requests
from requests_futures.sessions import FuturesSession

from ..custom_exceptions import api_exeption
from flask.ext.app.custom_exceptions.api_exeption import ExceptionManager
from ..lib.pyiCalendar import pyiCalendar


__author__ = 'Julien'


class AbstractCalendar(object):
    url_template = ""

    prof_tech = (
        "bauer", "braun", "dorwarth", "eidam", "eisenbiegler", "erb", "freudenmann", "haalboom", "hoffmann", "hoentsch",
        "ihle", "keller", "nick", "karin.schaefer", "schaefer", "schenkel", "schorr", "juergen.vollmer", "vollmer",
        "eric.zimmerman", "zidammerman"
    )

    prof_econ = (
        "becker", "borowicz", "detzel", "freytag", "grimm", "herold", "hochdoerffer", "hoffmann", "Junge", "kortschak",
        "roland.kuestermann", "kuestermann", "lehmeier", "mueller", "nold", "noelte", "pfannenschwarz", "rasch", "ratz",
        "schaefer", "wallrath", "weiland", "wengler"
    )

    programmes = {'TEL': 'dorwarth', 'TINF': 'freudenmann', 'TMB': None, 'TMT': None, 'TWIW': None, 'WHD': None,
                  'WIB': None, 'WWI': 'ratz'}

    def search_url(self, course):  # pragma: no cover
        program = re.findall("(\w+?)(?:\d+?B\d+?)", course)
        if program:
            prof = self.programmes.get(program[0])
            url = self.url_template.format(prof=prof, course=course)
            page = requests.head(url)
            if page.status_code == requests.codes.ok:
                return url

        prof_list = self.prof_tech if course.startswith("T") else self.prof_econ

        session = FuturesSession(max_workers=10)
        url_list = [self.url_template.format(**locals()) for prof in prof_list]
        responses = [session.head(u) for u in url_list]
        for response in responses:
            if response.result().status_code == requests.codes.ok:
                map(lambda elem: elem.cancel(), responses)
                return url

        return None

    def get_all_courses(self):  # pragma: no cover
        date = datetime.datetime.now()
        valid_courses = []

        year_end = int(datetime.datetime.now().strftime("%y"))
        year_start = year_end - 4

        for program, program_prof in self.programmes.items():
            prof_list = self.prof_tech if program.startswith("T") else self.prof_econ
            for year in range(year_start, year_end + 1):
                for group in range(1, 5):
                    course = "{}{}B{}".format(program, year, group)
                    if program_prof:
                        page = requests.head(self.url_template.format(prof=program_prof, course=course, date=date))
                        if page.status_code == requests.codes.ok:
                            valid_courses.append(course)
                    else:
                        for prof in prof_list:
                            page = requests.head(self.url_template.format(prof=prof, course=course, date=date))
                            if page.status_code == requests.codes.ok:
                                # TODO: add to self.programmes permanently
                                self.programmes[program] = prof
                                print(self.url_template.format(prof=prof, course=course, date=date))
                                program_prof = prof
                                valid_courses.append(course)

        return valid_courses

    def get_url(self, course, date=None):  # pragma: no cover
        program = re.findall("(\w+?)(?:\d+?B\d+?)", course)
        if not program:
            raise api_exeption.ExceptionManager.invalid_argument(100, "course", course)
        elif not self.programmes.get(program[0]):
            raise api_exeption.ExceptionManager.parse_error(101, course=course)
        else:
            if date is None:
                date = datetime.datetime.now()
            elif isinstance(date, basestring):
                date = datetime.datetime.strptime(date, "%Y-%m-%d")
            return self.url_template.format(prof=self.programmes.get(program[0]),
                                            course=course, date=date)


class IcalCalendar(AbstractCalendar):
    url_template = "https://rapla.dhbw-karlsruhe.de/rapla?page=iCal&user={prof}&file={course}"

    @staticmethod
    def _event_map(event):
        summary = event['SUMMARY'].decode('utf-8')
        summary = summary.replace("\\", "").strip("]")
        if len(summary.split(" [")) == 2:
            summary, person = summary.split(" [")
        else:
            person = ""

        temp = summary.split("TINF")
        if len(temp) >= 2:
            course = summary[len(temp[0]):].split(", ")
            summary = temp[0][:-1]
        else:
            course = [""]

        # "lastModified": event['LAST-MODIFIED']['val'].replace(tzinfo=tz.tzlocal()).isoformat(),
        return {
            "startTime": event['DSTART'].replace(tzinfo=tz.tzlocal()).isoformat(),
            "endTime": event['DTEND'].replace(tzinfo=tz.tzlocal()).isoformat(),
            "title": summary,
            "person": person,
            "course": course,
            "location": event['LOCATION']['val'].decode('utf-8').replace("\\", ""),
            "category": event['CATEGORIES']['val'].decode('utf-8')
        }

    def get_day(self, course, day):
        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(117, day=day)

        page = requests.get(self.get_url(course))

        if page.status_code != requests.codes.ok:
            raise api_exeption.ExceptionManager.parse_error(103, course=course)

        cal = pyiCalendar.iCalendar()
        cal.string_load(page.content)
        result = cal.get_event_instances(start=date.strftime("%Y%m%d"), end=date.strftime("%Y%m%d"))

        return map(self._event_map, result)

    def get_week(self, course, day):
        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(118, day=day)

        page = requests.get(self.get_url(course))

        if page.status_code != requests.codes.ok:
            raise api_exeption.ExceptionManager.parse_error(102, course=course)

        cal = pyiCalendar.iCalendar()
        cal.string_load(page.content)

        start = (date - datetime.timedelta(days=date.weekday())).strftime("%Y%m%d")
        end = (date + datetime.timedelta(days=6 - date.weekday())).strftime("%Y%m%d")

        result = cal.get_event_instances(start=start, end=end)

        return sorted(map(self._event_map, result), key=itemgetter("startTime"))

    def get_month(self, course, day):  # pragma: no cover
        page = requests.get(self.get_url(course))

        if page.status_code != requests.codes.ok:
            raise api_exeption.ExceptionManager.parse_error(102, course=course)

        cal = pyiCalendar.iCalendar()
        cal.string_load(page.content)

        # TODO: Catch value error
        day = datetime.datetime.strptime(day, "%Y-%m-%d")
        start_date = day - datetime.timedelta(days=day.day - 1)
        start = start_date.strftime("%Y%m%d")
        days_in_month = calendar.monthrange(day.year, day.month)[1]
        end = (start_date + datetime.timedelta(days=days_in_month - 1)).strftime("%Y%m%d")

        result = cal.get_event_instances(start=start, end=end)

        return sorted(map(self._event_map, result), key=itemgetter("startTime"))


class HtmlCalendar(AbstractCalendar):
    url_template = "https://rapla.dhbw-karlsruhe.de/rapla?page=calendar&" \
                   "user={prof}&file={course}&day={date.day}&month={date.month}&year={date.year}"

    @staticmethod
    def _event_map(event_tuple):
        date = event_tuple[0].replace(tzinfo=tz.tzlocal())
        event = event_tuple[1]

        result = {
            "startTime": None,
            "endTime": None,
            "title": None,
            "person": None,
            "course": None,
            "location": None,
            "category": event.find("strong").getText()
        }

        labels = map(lambda elem: elem.getText(), event.find_all("td", class_="label"))
        values = map(lambda elem: elem.getText(), event.find_all("td", class_="value"))

        # unused fields: Planungsnotiz, Änderungen erlauben, reserviert von
        field_dict = {
            u'Titel:': "title",
            u'Ressourcen:': "course",
            u'Personen:': "person"
        }

        for label, value in zip(labels, values):
            if label in field_dict:
                result[field_dict[label]] = value

        if result['course']:
            result['course'] = result['course'].split(",")
            result['location'] = max(result['course'], key=len)
            result['course'].remove(result['location'])

        # extra_dates = event.select("span.tooltip div")[0].getText().split(",")
        # extra_dates_format = lambda elem: parser.parse(elem, default=date, fuzzy=True).isoformat()
        # created, lastModified = map(extra_dates_format, extra_dates)

        date_text = event.select("span.tooltip div:nth-of-type(2)")[0].getText()
        time_range = re.findall(" (\d{2}:\d{2})-(\d{2}:\d{2}) ", date_text)
        if time_range:
            result['startTime'] = parser.parse(time_range[0][0], default=date).isoformat()
            result['endTime'] = parser.parse(time_range[0][1], default=date).isoformat()

        return result

    def get_day(self, course, day):
        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(116, day=day)
        week_start = (date - datetime.timedelta(days=date.weekday()))

        timedeltas = {day: datetime.timedelta(days=n) for n, day in
                      enumerate(["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"])}

        page = requests.get(self.get_url(course, date))

        if page.status_code != requests.codes.ok:
            raise api_exeption.ExceptionManager.parse_error(107, course=course)

        soup = BeautifulSoup(page.content)

        events = soup.find_all("td", class_="week_block")
        time_blocks = map(lambda week_block: week_block.select("span.tooltip div:nth-of-type(2)")[0].getText(), events)
        get_date = lambda time_text: None if timedeltas.get(time_text[:2]) is None else week_start + timedeltas.get(
            time_text[:2])
        correct_date = lambda (elem, elem2): elem is not None and elem.isocalendar() == date.isocalendar()
        events = filter(correct_date, zip(map(get_date, time_blocks), events))

        return sorted(map(self._event_map, events), key=itemgetter("startTime"))

    def get_week(self, course, day):
        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(119, day=day)
        week_start = (date - datetime.timedelta(days=date.weekday()))
        get_weekday = lambda day_number: (week_start + datetime.timedelta(days=day_number)).isocalendar()
        days = [get_weekday(day_number) for day_number in range(7 - week_start.weekday())]

        timedeltas = {day: datetime.timedelta(days=n) for n, day in
                      enumerate(["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"])}

        page = requests.get(self.get_url(course, date))

        if page.status_code != requests.codes.ok:
            raise api_exeption.ExceptionManager.parse_error(108, course=course)

        soup = BeautifulSoup(page.content)

        events = soup.find_all("td", class_="week_block")
        time_blocks = map(lambda week_block: week_block.select("span.tooltip div:nth-of-type(2)")[0].getText(), events)
        get_date = lambda time_text: None if timedeltas.get(time_text[:2]) is None else week_start + timedeltas.get(
            time_text[:2])
        correct_date = lambda (elem, elem2): elem is not None and elem.isocalendar() in days
        events = filter(correct_date, zip(map(get_date, time_blocks), events))

        return sorted(map(self._event_map, events), key=itemgetter("startTime"))
