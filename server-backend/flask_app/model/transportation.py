# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from dateutil import parser, tz
import requests

from ..custom_exceptions.api_exeption import InvalidAPIUsage, ExceptionManager


__author__ = 'Julien'


class AbstractTransportation(object):
    color_dict = {"3": "#9a763a", "NL3": "#9a763a"}

    @staticmethod
    def _now():
        """ :return: datetime.datetime """
        return datetime.datetime.now(tz.tzlocal())


class KvvApiTransportation(AbstractTransportation):
    api_base = "http://live.kvv.de/webapp"
    payload = {"key": "377d840e54b59adbe53608ba1aad70e8"}

    @classmethod
    def _convert_time(cls, old_time):
        """
        Converts time from formats like "4 min" or "14:45" to an ISO 8601 time format.

        :param old_time: The time returned by the Live KVV API
        :type old_time: str
        :return: The time in the uniform time format
        :rtype: str
        """
        # time can be returned as "4 min"
        # this is converted to a date string
        time_split = old_time.strip().split(" ")
        current_time = cls._now().replace(second=0, microsecond=0)
        if len(time_split) == 2 and time_split[0].isdigit():
            new_time = current_time + datetime.timedelta(minutes=int(time_split[0]))
        else:
            new_time = parser.parse(old_time, default=current_time)

            # if the new_time is in the past it probably for the next day like "00:52"
            if new_time < current_time:
                new_time = new_time + datetime.timedelta(days=1)

        return new_time.isoformat()

    @classmethod
    def _format_connection(cls, old_connection):
        """
        Simplifies the connection information returned by the Live KVV API

        :param old_connection: the connection in the old format
        :type old_connection: dict[unicode, unicode]
        :return: the connection in the new format with the keys
          line, destination, time and lineColor
        :rtype: dict[unicode, unicode]
        """
        return {
            "line": old_connection["route"],
            "destination": old_connection["destination"],
            "time": cls._convert_time(old_connection["time"]),
            # color defaults to gray if it wasn't found in the color_dict
            "lineColor": cls.color_dict.get(old_connection["route"], "#a4a4a4")
        }

    @staticmethod
    def _city_direction(connection):
        """
        Checks if the connection is in the direction of the city

        :param connection: the connection
        :type connection: dict[str, str]
        :return: if the statement is true
        :rtype: bool
        """
        return connection["direction"] == "2"

    def get_next(self):
        url = self.api_base + "/departures/bystop/de:8212:12"
        page = requests.get(url, params=self.payload, timeout=4)
        if page.status_code != requests.codes.ok:
            raise ExceptionManager.parse_error(111, "Couldn't load page for next transporation.")

        departures = page.json()["departures"]
        result = [connection for connection in departures if self._city_direction(connection)]
        return [self._format_connection(connection) for connection in result]

    def search_by_name(self, name):  # pragma: no cover
        url = self.api_base + "/stops/byname/" + name
        page = requests.get(url, params=self.payload, timeout=4)
        if page.status_code != requests.codes.ok:
            raise InvalidAPIUsage(112, "Couldn't load page for search.", page.status_code)

        reduce_element = lambda elem: {"id": elem["id"], "name": elem["name"]}
        return [reduce_element(stop) for stop in page.json()["stops"]]

    def search_by_location(self, lat, lon):  # pragma: no cover
        url = "{}/stops/bylatlon/{}/{}".format(self.api_base, lat, lon)
        page = requests.get(url, params=self.payload, timeout=4)
        if page.status_code != requests.codes.ok:
            raise InvalidAPIUsage(113, "Couldn't load page for search.", page.status_code)

        reduce_element = lambda elem: {
            "id": elem["id"],
            "name": elem["name"],
            "distance": elem["distance"]
        }

        return [reduce_element(stop) for stop in page.json()["stops"]]
