#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os
import sqlite3
import json


class JsonRooms(object):
    def get_rooms(self):
        path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'rooms.json'))
        with open(path) as f:
            return json.load(f)


class SqliteRooms(object):

    query = """
    SELECT raum.Raumnummer, raum.Stock, raumbezeichnung.Raumbezeichnung, lokalisation.Lokalisation, raumbereich.Fluegel
    FROM raum
      JOIN raumbezeichnung ON raum.FKRaumbezeichnung = raumbezeichnung.IDRaumbezeichnung
      JOIN lokalisation  ON raum.FKLokalisation = lokalisation.IDLokalisation
      JOIN raumbereich ON raum.FKFluegel = raumbereich.IDFluegel
    """

    def _format_room(self, room):
        return {
            "roomNr": room[0] or "",
            "floor": room[1] or -1,
            "description": room[2] or "",
            "hallwayPosition": room[3] or "",
            "hallway": room[4] or ""
        }

    def get_rooms(self):
        path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'room.sqlite'))
        conn = sqlite3.connect(path)
        c = conn.cursor()
        c.execute(self.query)
        results = c.fetchall()

        return map(self._format_room, results)


