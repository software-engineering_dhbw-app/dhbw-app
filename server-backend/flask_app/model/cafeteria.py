# coding=utf-8
from __future__ import unicode_literals
import datetime
import re
from operator import itemgetter

from bs4 import BeautifulSoup
import requests

from ..custom_exceptions.api_exeption import ExceptionManager


__author__ = 'Julien'


class AbstractCafeteria(object):
    @staticmethod
    def _format_date(date, day_offset=0, format_="%Y-%m-%d"):
        if day_offset:
            date = date + datetime.timedelta(days=day_offset)
        return date.strftime(format_) if format_ else date

    @classmethod
    def _get_weekdays(cls, date, format_="%Y-%m-%d"):
        """

        :param date:
        :type date: datetime.datetime
        :param format_:
        :type format_: str|None
        :return:
        :rtype: list[str|datetime.datetime]
        """
        start = (date - datetime.timedelta(days=date.weekday()))
        total_days = 7

        # Don't show already passed days
        days_in_past = cls._now() - start
        if days_in_past > datetime.timedelta():
            start = start + days_in_past
            total_days -= days_in_past.days

        return [cls._format_date(start, day_number, format_) for day_number in range(total_days)]

    @staticmethod
    def _now():
        """ :return: datetime.datetime """
        return datetime.datetime.now()


class JsonCafeteria(AbstractCafeteria):

    @staticmethod
    def _format_meal(meal):
        result = {
            "name": meal["name"],
            "category": meal["category"],
            "studentPrice": meal["prices"]["students"],
            "condiments": []
        }

        name_split = result["name"].split("[")
        if len(name_split) == 2:
            result["name"] = name_split[0]
            result["condiments"] = sorted(name_split[1][:-1].split(","))

        if result['studentPrice'] is None:
            result['studentPrice'] = -1

        return result

    @staticmethod
    def _format_day(day):
        meals = [JsonCafeteria._format_meal(meal) for meal in day.get("meals", [])]
        meals = sorted(meals, key=itemgetter("studentPrice", "category"), reverse=True)

        return {
            "date": day["date"],
            "closed": day.get("closed", True),
            "meals": meals
        }

    @staticmethod
    def get_open_days():
        page = requests.get("http://openmensa.org/api/v2/canteens/33/days/", timeout=4)
        if page.status_code != requests.codes.ok:
            raise ExceptionManager.parse_error(105, canteen=page.status_code)

        return [day['date'] for day in page.json()]

    def get_day_plan(self, day):
        url = "http://openmensa.org/api/v2/canteens/33/days/{0}/meals".format(day)
        page = requests.get(url)
        if page.status_code == 200:
            meals = [self._format_meal(meal) for meal in page.json()]
            meals = sorted(meals, key=itemgetter("studentPrice", "category"), reverse=True)
            return {"date": day, "closed": False, "meals": meals}
        elif page.status_code == 404:
            # when the canteen is closed, the api will just respond with an 404 error code
            return {"date": day, "closed": True, "meals": []}
        else:
            raise ExceptionManager.parse_error(104, day=day)

    def get_week_plan(self, day):
        url = "http://openmensa.org/api/v2/canteens/33/meals".format(day)
        page = requests.get(url)
        """:type: requests.Response"""
        if page.status_code != requests.codes.ok:
            raise ExceptionManager.parse_error(104, day=day)

        result = page.json()
        days = {elem["date"]: elem for elem in result}

        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(113, day=day)

        result = []

        if date - self._now() >= datetime.timedelta(days=7):
            return result

        for weekday in self._get_weekdays(date):
            day_dict = days.get(weekday, {"date": weekday})
            result.append(self._format_day(day_dict))

        return sorted(result, key=itemgetter("date"))


class HtmlCafeteria(AbstractCafeteria):
    @staticmethod
    def _format_meals(meals):
        """

        :param meals:
        :type meals: BeautifulSoup.Tag
        :return: dict
        """
        result = []
        for category in meals.find_all("td", width="20%"):
            inner_table = category.find_next("table")

            meal_column = inner_table.find_all("td", class_="first")
            price_column = inner_table.find_all("span", class_=["bgp", "price_1", "bg"],
                                                text=re.compile(u"\u20AC"))

            for meal_total, price_total in zip(meal_column, price_column):
                meal_total = meal_total.find("span").getText().strip().split("\n")
                if "(" in meal_total[-1]:
                    condiments = sorted(meal_total[-1].strip(" ()").split(","))
                    meal_name = meal_total[:-1]
                else:
                    condiments = []
                    meal_name = meal_total
                meal_name = " ".join(meal_name).strip()

                price = -1
                for elem in price_total.getText().split(" "):
                    if "," in elem and elem.replace(",", ".")[0].isdigit():
                        price = float(elem.replace(",", "."))



                result.append({
                    "name": meal_name,
                    "category": category.getText(),
                    "studentPrice": price,
                    "condiments": condiments
                })

        return sorted(result, key=itemgetter("studentPrice", "category"), reverse=True)

    @classmethod
    def _format_day(cls, day_html, date):
        meals = HtmlCafeteria._format_meals(day_html.find_next("table")) if day_html else []
        meals = sorted(meals, key=itemgetter("studentPrice", "category"), reverse=True)

        return {
            "date": cls._format_date(date),
            "closed": not bool(day_html),
            "meals": meals
        }

    def get_day_plan(self, day):
        url = "http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw={}"

        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(114, day=day)

        if self._now() - date >= datetime.timedelta(days=1):
            raise ExceptionManager.parse_error(115, day=day, extra_text="The day is in the past.")

        week = date.isocalendar()[1]

        day_regex = re.compile(date.strftime("%d\\.%m\\."))

        page = requests.get(url.format(week))
        """:type: requests.Response"""
        if page.status_code != requests.codes.ok:
            raise ExceptionManager.parse_error(106, day=day)

        soup = BeautifulSoup(page.content)

        return self._format_day(soup.find(text=day_regex), date)

    def get_week_plan(self, day):
        url = "http://www.sw-ka.de/de/essen/?view=ok&STYLE=popup_plain&c=erzberger&p=1&kw={}"

        try:
            date = datetime.datetime.strptime(day, "%Y-%m-%d")
        except ValueError:
            raise ExceptionManager.parse_error(115, day=day)
        week = date.isocalendar()[1]

        page = requests.get(url.format(week))
        """:type: requests.Response"""
        if page.status_code != requests.codes.ok:
            raise ExceptionManager.parse_error(106, day=day)

        soup = BeautifulSoup(page.content)

        result = []

        for weekday in self._get_weekdays(date, format_=None):
            if weekday - self._now() > datetime.timedelta(days=52):
                continue

            day_html = soup.find("h1", text=re.compile(weekday.strftime("%d\\.%m\\.")))
            result.append(self._format_day(day_html, weekday))

        return sorted(result, key=itemgetter("date"))
