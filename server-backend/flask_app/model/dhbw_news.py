# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from operator import itemgetter

from dateutil import parser
import feedparser


__author__ = 'Julien'


class RssDhbwNews(object):
    url = 'http://www.dhbw-karlsruhe.de/index.php?id=313&type=100'

    @staticmethod
    def _format_article(entry):
        return {"title": entry.title,
                "link": entry.link,
                "description": entry.description,
                "published": parser.parse(entry.published).isoformat(),
                "content": entry.content[0].value}

    @classmethod
    def get_news(cls):
        feed = feedparser.parse(cls.url)
        result = [cls._format_article(article) for article in feed.entries]
        return sorted(result, key=itemgetter("published"), reverse=True)
