# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from operator import itemgetter
from bs4 import BeautifulSoup

from dateutil import parser
import feedparser


__author__ = 'Julien'


class FacebookStuvNews(object):
    url = 'https://www.facebook.com/feeds/page.php?format=rss20&id=166591256706746'

    @staticmethod
    def _format_article(entry):
        title = entry.title.split("\n")[0].strip(" *_+=")
        title = BeautifulSoup(title).text
        return {
            "title": title,
            "link": entry.link,
            "content": entry.description,
            "published": parser.parse(entry.published).isoformat()
        }

    @classmethod
    def get_news(cls):
        feed = feedparser.parse(cls.url)
        result = [cls._format_article(article) for article in feed.entries if article["title"]]
        return sorted(result, key=itemgetter("published"), reverse=True)
