# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from bs4 import BeautifulSoup
import requests

__author__ = 'Julien'


class HomepageContacts(object):
    url = "https://www.dhbw-karlsruhe.de/allgemein/dhbw-karlsruhe/mitarbeiterverzeichnis/"

    @staticmethod
    def _format_contact(contact):
        name, room, telephone, function = contact

        return {
            "name": name,
            "room": room,
            "telephone": telephone.replace(".", " "),
            "function": function
        }

    @classmethod
    def get_contact(cls):
        page = requests.get(cls.url)
        soup = BeautifulSoup(page.text)

        table_body = soup.find('div', id='content2')

        result = []

        contact_rows = table_body.find_all('tr')
        for contact_row in contact_rows:
            contact_column = [column.text.strip() for column in contact_row.find_all('td')]

            # Get rid of empty values
            if any(contact_column):
                result.append(cls._format_contact(contact_column))

        return result
