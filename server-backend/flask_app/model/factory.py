# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .cafeteria import JsonCafeteria, HtmlCafeteria
from .calendar_ import IcalCalendar, HtmlCalendar
from .contact import HomepageContacts
from ..custom_exceptions import api_exeption
from ..model.rooms import SqliteRooms, JsonRooms
from .stuv_news import FacebookStuvNews
from .transportation import KvvApiTransportation
from .dhbw_news import RssDhbwNews


__author__ = 'Julien'


class AbstractFactory(object):
    implementations = []

    @classmethod
    def iterate_implementations(cls, method_name, *args, **kwargs):
        fallback = kwargs.get("fallback", [])
        combine_method = kwargs.get("combine_method", None)
        implementation_order = kwargs.get("implementation_order", cls.implementations)

        first_error = None
        total_result = None
        for implementation in implementation_order:
            try:
                func = getattr(implementation, method_name)
            except AttributeError:
                continue

            try:
                result = func(*args)
                if result and combine_method:
                    func = getattr(implementation, combine_method)
                    total_result = func(total_result, result)
                elif result:
                    total_result = result
                    break
            except api_exeption.InvalidAPIUsage as e:
                if first_error is None:
                    first_error = e
        if total_result:
            return total_result
        if first_error is not None:
            raise first_error
        else:
            return fallback


class CafeteriaFactory(AbstractFactory):
    implementations = [JsonCafeteria(), HtmlCafeteria()]

    @classmethod
    def get_day_plan(cls, day):
        return cls.iterate_implementations("get_day_plan", day)

    @classmethod
    def get_week_plan(cls, day):
        return cls.iterate_implementations("get_week_plan", day)

    @classmethod
    def get_open_days(cls):
        return cls.iterate_implementations("get_open_days")


class TransportationFactory(AbstractFactory):
    implementations = [KvvApiTransportation()]

    @classmethod
    def get_next(cls):
        return cls.iterate_implementations("get_next")

    @classmethod
    def search_by_name(cls, name):
        return cls.iterate_implementations("search_by_name", name)

    @classmethod
    def search_by_location(cls, lat, lon):
        return cls.iterate_implementations("search_by_location", lat, lon)


class CalendarFactory(AbstractFactory):
    implementations = [IcalCalendar(), HtmlCalendar()]

    @classmethod
    def get_day(cls, course, day):
        return cls.iterate_implementations("get_day", course, day)

    @classmethod
    def get_week(cls, course, day):
        return cls.iterate_implementations("get_week", course, day)

    @classmethod
    def get_month(cls, course, day):
        return cls.iterate_implementations("get_month", course, day)

    @classmethod
    def get_all_courses(cls):
        custom_order = [HtmlCalendar(), IcalCalendar()]
        return cls.iterate_implementations("get_all_courses", implementation_order=custom_order)


class StuvNewsFactory(AbstractFactory):
    implementations = [FacebookStuvNews()]

    @classmethod
    def get_news(cls):
        return cls.iterate_implementations("get_news")


class DhbwNewsFactory(AbstractFactory):
    implementations = [RssDhbwNews()]

    @classmethod
    def get_news(cls):
        return cls.iterate_implementations("get_news")


class ContactFactory(AbstractFactory):
    implementations = [HomepageContacts()]

    @classmethod
    def get_contact(cls):
        return cls.iterate_implementations("get_contact")


class RoomsFactory(AbstractFactory):
    implementations = [JsonRooms(), SqliteRooms()]

    @classmethod
    def get_rooms(cls):
        return cls.iterate_implementations("get_rooms")