# -*- coding: utf-8 -*-
import logging

from flask import Flask, jsonify, make_response, render_template
from werkzeug.contrib.fixers import ProxyFix

from config import load_config
from .custom_exceptions import api_exeption
from .utils.cache import cache

__version__ = "v2"


def create_app():
    """Create Flask app."""
    app = Flask(__name__)

    cache.init_app(app)

    configuration = load_config()
    app.config.from_object(configuration)

    if not hasattr(app, 'production'):
        app.production = not app.debug and not app.testing

    if not app.debug:
        # In production mode, add log handler to sys.stderr.
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.INFO)

    # Proxy fix
    app.wsgi_app = ProxyFix(app.wsgi_app)

    # Register components
    register_routes(app)
    register_error_handle(app)

    return app


def register_routes(app):
    """Register routes."""
    from flask_app.views import cafeteria, calendar_, contact, dhbw_news, stuv_news, transportation, rooms

    app.register_blueprint(cafeteria.blueprint)
    app.register_blueprint(calendar_.blueprint)
    app.register_blueprint(contact.blueprint)
    app.register_blueprint(dhbw_news.blueprint)
    app.register_blueprint(stuv_news.blueprint)
    app.register_blueprint(transportation.blueprint)
    app.register_blueprint(rooms.blueprint)

    from .views import request_wants_json, get_endpoint

    @app.route(get_endpoint("/"))
    def help_text():
        u"""
        Gives an overview of how the API can be used. It returns all available
        endpoints and the corresponding documentation (which is shown here).
        If the Accept-Header requests JSON, then it will return the information as
        an JSON object. Otherwise it will return as a very simple HTML page.

        The documentation is always returned as unformatted restructedText_ with
        an added Sphinx_ domain provided by the plugin sphinxcontrib.autohttp.flask_.

        .. _restructedText: http://sphinx-doc.org/rest.html

        .. _Sphinx: http://sphinx-doc.org/index.html

        .. _sphinxcontrib.autohttp.flask:
            http://pythonhosted.org/sphinxcontrib-httpdomain/#sphinxcontrib-autohttp-flask-exporting-api-reference-from-flask-app

        **Example request with JSON**:

        .. sourcecode:: http

            GET /dhbw-api/v2/ HTTP/1.1
            Accept: application/json

        **Example response with JSON**:

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: application/json;charset=utf-8

            {
                "/dhbw-api/v2/cafeteria/week/": "\

            ...


        **Example request with HTML**:

        .. sourcecode:: http

            GET /dhbw-api/v2/ HTTP/1.1

        **Example response with HTML** (truncated):

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/html;charset=utf-8

            <html>
            <head><title>DHBW-API Schnittstelle</title></head>
            <body>

            <h1><a href="/v2/calendar/&lt;course&gt;/week/&lt;day&gt;/">
            /v2/calendar/</a>
            </h1>

            <p>This is a work in progress.</p>


            <h1><a href="/dhbw-api/v2/cafeteria/week/">/dhbw-api/v2/cafeteria/week/</a></h1>
            <p><br>
                Meals that are served today in the cafeteria.<br>
            <br>
                **Example request**:<br>
            <br>
            </body>
            </html>

        :reqheader Accept: The response depends on the :http:header:`Accept` header.
          If :mimetype:`application/json` is specified, then json will be returned;
          :mimetype:`text/html` otherwise.
        """
        func_list = {}
        for rule in app.url_map.iter_rules():
            if rule.endpoint != 'static':
                documentation = app.view_functions[rule.endpoint].__doc__
                func_list[rule.rule] = documentation if documentation is not None else ""

        if request_wants_json():
            return make_response(func_list)
        else:
            # noinspection PyUnresolvedReferences
            return render_template("all_links.html", links=func_list)


def register_error_handle(app):
    """Register HTTP error pages."""

    @app.errorhandler(api_exeption.InvalidAPIUsage)
    def handle_invalid_usage(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response



