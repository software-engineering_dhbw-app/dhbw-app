#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os


class Config(object):
    """Base config class."""
    # Flask app config
    DEBUG = False
    TESTING = False

    # Root path of project
    PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    BIND_ADDRESS = "0.0.0.0"

    # Site domain
    SITE_TITLE = "DHBW-API Schnittstelle"


class DevelopmentConfig(Config):
    # App config
    DEBUG = True


class TestingConfig(Config):
    # App config
    TESTING = True


class ProductionConfig(Config):

    BIND_ADDRESS = "127.0.0.1"
