corepkgs:
   pkg.installed:
    - pkgs:
      - tmux
      - python-dev
      - python-virtualenv
      - python-pip

{{ pillar['flask-home'] }}/flask-venv:
  virtualenv.managed:
    - system_site_packages: False
    - requirements: {{ pillar['project-home'] }}/requirements.txt

start-flask:
  cmd.run:
    - name: {{ pillar['flask-home'] }}/flask-venv/bin/gunicorn server-application:app
        --daemon
        --debug --log-level info
        --reloader
        --workers 3
        --bind 127.0.0.1:5000
        --pid {{ pillar['flask-home'] }}/PIDFILE
        --access-logfile {{ pillar['flask-home'] }}/access.log
        --error-logfile {{ pillar['flask-home'] }}/error.log
    - cwd: {{ pillar['project-home'] }}
    - creates: {{ pillar['flask-home'] }}/PIDFILE