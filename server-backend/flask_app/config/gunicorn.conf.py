import os

__author__ = 'Julien'

import multiprocessing

daemon = False
debug = True
log_level = "debug"
workers = multiprocessing.cpu_count() * 2 + 1
bind = "0.0.0.0:5000"
reloader = True
pidfile = '/tmp/gunicorn_flask_app.pid'

PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
logconfig = os.path.abspath(os.path.join(PROJECT_PATH, "config/gunicorn_logging.conf"))