#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from . import flask_config


def load_config():
    """Load config."""
    mode = os.environ.get('MODE')

    if mode == 'PRODUCTION':
        return flask_config.ProductionConfig
    elif mode == 'TESTING':
        return flask_config.TestingConfig
    else:
        return flask_config.DevelopmentConfig


