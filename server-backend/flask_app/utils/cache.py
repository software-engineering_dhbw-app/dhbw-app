#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from flask.ext.cache import Cache

cache_config = {'CACHE_TYPE': 'simple'}
if os.environ.get('DB_NAME'):
    cache_config['CACHE_TYPE'] = 'redis'
    cache_config['CACHE_REDIS_HOST'] = 'db'

cache = Cache(config=cache_config)
