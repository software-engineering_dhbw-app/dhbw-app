import json
import pprint

from behave import *
import requests


@when(u'I want to see the meals for today.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/cafeteria/day/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the meals for the day "{day}".')
def step_impl(context, day):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/cafeteria/day/{}/".format(day))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the meals for this week.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/cafeteria/week/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the meals for the week containing "{day}".')
def step_impl(context, day):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/cafeteria/week/{}/".format(day))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the events of "{course}" for the day "{day}".')
def step_impl(context, course, day):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/calendar/{}/day/{}/".format(course, day))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the events of "{course}" for the week containing "{day}".')
def step_impl(context, course, day):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/calendar/{}/week/{}/".format(course, day))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the events of "{course}" for today.')
def step_impl(context, course):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/calendar/{}/day/".format(course))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see the events of "{course}" for this week.')
def step_impl(context, course):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/calendar/{}/week/".format(course))
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see all DHBW news articles.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/dhbw-news/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see all StuV news articles.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/dhbw-news/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see all contact persons.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/contact/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@when(u'I want to see  the next trains.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    context.response = context.app.get("/v2/transportation/")
    assert context.response.status_code == requests.codes.ok, \
        "Status code: {}".format(context.response.status_code)

    context.json = json.loads(context.response.data)
    pprint.pprint(context.json)


@then(u'I will see at least one meal.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    assert len(context.json["meals"]) > 0


@then(u'I will see at least one event.')
@then(u'I will see at least one day.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    assert len(context.json) > 0


@then(u'I will see at least one meal if open.')
def step_impl(context):
    """:type context: behave.runner.Context"""
    assert context.json["closed"] or len(context.json["meals"]) > 0


@then(u'I want to see at least the json keys "{keys}".')
def step_impl(context, keys):
    """:type context: behave.runner.Context"""
    key_list = keys.split(",")
    for key in key_list:
        assert key.strip() in context.json, "Missing key {}.".format(key.strip())


@then(u'I want to see a list with json elements having least the keys "{keys}".')
def step_impl(context, keys):
    """:type context: behave.runner.Context"""
    key_list = keys.split(",")
    for n, element in enumerate(context.json):
        for key in key_list:
            assert key.strip() in element, "{} missing for element {}..".format(key.strip(), n)


@then(u'I want to see the key "{key}" containing a list.')
def step_impl(context, key):
    """:type context: behave.runner.Context"""
    assert key in context.json, "Missing key {}.".format(key)
    assert isinstance(context.json[key], list), "Key {} doesn't contain a list".format(key)


@then(u'I want to see a list with json elements with the key "{key}" containing a list.')
def step_impl(context, key):
    """:type context: behave.runner.Context"""
    for elem in context.json:
        assert key in elem, "Missing key {}.".format(key)
        assert isinstance(elem[key], list), "Key {} doesn't contain a list".format(key)


@then(u'I want the list for the key "{key}" to contain at least the keys "{keys}".')
def step_impl(context, key, keys):
    """:type context: behave.runner.Context"""
    key_list = keys.split(",")
    for n, element in enumerate(context.json[key]):
        for key in key_list:
            assert key.strip() in element, "{} missing for element {} in key {}..".format(key.strip(), n, key)


@then(u'I will see an error.')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then I will see an error.')