@cafeteria @week
Feature:
  As a website owner
  I want to show the meals of a specified week

  Scenario: Meals are available for week view.
    When I want to see the meals for the week containing "2015-06-20".
    Then I will see at least one day.
    Then I want to see a list with json elements having least the keys "date, meals, closed".
    Then I want to see a list with json elements with the key "meals" containing a list.

  @today
  Scenario: Show menu for this week.
    When I want to see the meals for this week.
    Then I will see at least one day.
    Then I want to see a list with json elements having least the keys "date, meals, closed".
    Then I want to see a list with json elements with the key "meals" containing a list.

