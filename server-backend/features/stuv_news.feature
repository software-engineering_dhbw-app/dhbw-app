@contact
Feature:
  As a website owner
  I want to see a the be able to fetch all StuV news articles

  Scenario: Show StuV news articles.
    When I want to see all StuV news articles.
    Then I want to see a list with json elements having least the keys "content, published, link, description, title".
