@contact
Feature:
  As a website owner
  I want to see a the be able to fetch all DHBW news articles

  Scenario: Show DHBW news articles.
    When I want to see all DHBW news articles.
    Then I want to see a list with json elements having least the keys "content, published, link, description, title".
