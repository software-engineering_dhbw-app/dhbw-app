@calendar @week
Feature:
  As a website owner
  I want to show the events of a specified week

  Scenario: Show events for a week
    When I want to see the events of "TINF13B2" for the week containing "2014-12-08".
    Then I will see at least one event.
    Then I want to see a list with json elements having least the keys "category, person, course, location, startTime, title, endTime".
    #Then I want to see the key "course" containing a list.

  @today
  Scenario: Show events for this week
    When I want to see the events of "TINF13B2" for this week.
    Then I want to see a list with json elements having least the keys "category, person, course, location, startTime, title, endTime".