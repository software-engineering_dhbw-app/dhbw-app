@contact
Feature:
  As a website owner
  I want to see a list of all contact persons

  Scenario: Show contact persons.
    When I want to see all contact persons.
    Then I want to see a list with json elements having least the keys "telephone, function, name, room".
