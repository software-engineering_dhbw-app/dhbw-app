@cafeteria @day
Feature:
  As a website owner
  I want to show the meals of a specified day

  Scenario: Meals are available for day view.
    When I want to see the meals for the day "2014-12-08".
    Then I will see at least one meal.
    Then I want to see at least the json keys "date, meals, closed".
    Then I want to see the key "meals" containing a list.
    Then I want the list for the key "meals" to contain at least the keys "name, studentPrice".

  @today
  Scenario: Show menu for today.
    When I want to see the meals for today.
    Then I will see at least one meal if open.
    Then I want to see at least the json keys "date, meals, closed".
    Then I want to see the key "meals" containing a list.
    Then I want the list for the key "meals" to contain at least the keys "name, studentPrice".

