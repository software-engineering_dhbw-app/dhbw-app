# -*- coding: utf-8 -*-
import application


def before_all(context):
    """:type context: behave.runner.Context"""
    application.app.config['TESTING'] = True
    context.app = application.app.test_client()