import datetime
import requests

__author__ = 'jhadleyjack'

url_template = "https://rapla.dhbw-karlsruhe.de/rapla?page=iCal&user={prof}&file={course}"

prof_tech = ("bauer", "braun", "dorwarth", "eidam", "eisenbiegler", "erb", "freudenmann", "haalboom", "hoffmann", "hoentsch", "ihle", "keller", "nick", "karin.schaefer", "schaefer", "schenkel", "schorr", "juergen.vollmer", "vollmer", "eric.zimmerman", "zimmerman")
prof_econ = ("becker", "borowicz", "detzel", "freytag", "grimm", "herold", "hochdoerffer", "hoffmann", "Junge", "kortschak", "roland.kuestermann", "kuestermann", "lehmeier", "mueller", "nold", "noelte", "pfannenschwarz", "rasch", "ratz", "schaefer", "wallrath", "weiland", "wengler")

programmes = {'TEL': 'dorwarth', 'TINF': 'freudenmann', 'TMB': None, 'TMT': None, 'TWIW': None, 'WHD': None, 'WIB': None, 'WWI': 'ratz'}

year_end = int(datetime.datetime.now().strftime("%y")) + 1
year_start = year_end - 5

for program, program_prof in programmes.items():
    print("\n" + program)
    prof_list = prof_tech if program.startswith("T") else prof_econ
    for year in range(year_start, year_end):
        for group in range(0, 5):
            course = "{}{}B{}".format(program, year, group)
            if program_prof:
                page = requests.head(url_template.format(prof=program_prof, course=course))
                if page.status_code == requests.codes.ok:
                    print(url_template.format(prof=program_prof, course=course))
            else:
                for prof in prof_list:
                    page = requests.head(url_template.format(**locals()))
                    if page.status_code == requests.codes.ok:
                        print(url_template.format(**locals()))
                        program_prof = prof


