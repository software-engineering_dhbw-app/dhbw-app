# -*- coding: utf-8 -*-
from flask_app import create_app

app = create_app()


def run():
    app.run(host=app.config['BIND_ADDRESS'])


if __name__ == '__main__':
    run()
