# Inofficial DHBW-API

To simplify access to a lot of information that students need, you can use this API.

## Testing

To be able to test the API on a local machine we recommend using [Docker](https://www.docker.com/). After installation you only need to run:

```
sudo docker run --rm -p 5000:5000 --name dhbw-api pilosafolivora/dhbw-api
```

This starts a container. You can test to see if it its working by visiting: http://localhost:5000/v2/transportation/

## Developing

For developing we recommend [Docker](https://www.docker.com/) combined with [Docker Compose](https://docs.docker.com/compose/).

Clone the repository:

```
git clone https://bitbucket.org/software-engineering_dhbw-app/dhbw-app.git
```

And run Docker Compose in the folder for the API
```
cd dhbw-app/server-backend
sudo docker-compose up
```

This starts a container. You can test to see if it its working by visiting: http://localhost:5000/v2/transportation/